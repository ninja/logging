﻿using System.Collections.Generic;
using Modomodo.Portal.Logging.Model.Models;

namespace Modomodo.Portal.Logging.Data.Interfaces
{
    public interface IFilterRepository : IDataRepository
    {
        bool Insert(Filter filter);
        bool Delete(Filter filter);
        Filter WhereByFilterName(string filterName);
        IEnumerable<Filter> WhereByApplicationName(string applicationName);
        bool UpdateBody(Filter filter);
    }
}