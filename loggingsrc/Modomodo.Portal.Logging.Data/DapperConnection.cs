﻿using System;
using System.Data;

namespace Modomodo.Portal.Logging.Data
{
    public class DapperConnection : IDisposable
    {
        private readonly IDbConnection _connection;

        public DapperConnection(IDbConnection connection)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));            
            _connection = connection;
        }

        public IDbConnection Connection
        {
            get
            {
                if (_connection.State != ConnectionState.Open && _connection.State != ConnectionState.Connecting)
                    _connection.Open();
                return _connection;
            }
        }

        public void Dispose()
        {
            if (_connection != null && _connection.State != ConnectionState.Closed)
                _connection.Close();
        }
    }
}
