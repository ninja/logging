﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Modomodo.Portal.Logging.Data.Interfaces;
using Modomodo.Portal.Logging.Model.Models;

namespace Modomodo.Portal.Logging.Data
{
    public class FilterRepository : DataRepository, IFilterRepository
    {
        private readonly string _tableName = "[PortalLogging].[dbo].[Filters]";

        public FilterRepository(IDbConnection connection) : base(connection)
        {
        }

        public bool Insert(Filter filter)
        {
            ValidationModel(filter);

            var statementSql = $"INSERT INTO {_tableName} (Name, Body, ApplicationName) VALUES(@Name, @Body, @ApplicationName)";
            return Connection.Execute(statementSql, new { Name = filter.Name, Body = filter.Body, ApplicationName = filter.ApplicationName }) > 0;
        }

        private void ValidationModel(Filter filter)
        {
            if (filter == null)
                throw new ArgumentNullException(nameof(filter));
        }

        public bool Delete(Filter filter)
        {
            ValidationModel(filter);
            
            var statementSql = $"DELETE FROM {_tableName} WHERE Name = @Name AND ApplicationName = @ApplicationName";
            return Connection.Execute(statementSql, new {Name = filter.Name, ApplicationName = filter.ApplicationName}) > 0;
        }

        public Filter WhereByFilterName(string filterName)
        {
            if( string.IsNullOrWhiteSpace( filterName))
                throw new ArgumentNullException(nameof(filterName));
            var statementSql = $"SELECT * FROM {_tableName} WHERE Name = @filterName";
            var query = Connection.Query<Filter>(statementSql, new {filterName});
            return query.FirstOrDefault();
        }

        public IEnumerable<Filter> WhereByApplicationName(string applicationName)
        {
            if( string.IsNullOrWhiteSpace( applicationName))
                throw new ArgumentNullException( nameof(applicationName));
            var statementSql = $"SELECT * FROM {_tableName} WHERE ApplicationName = @applicationName";
            var query = Connection.Query<Filter>(statementSql, new {applicationName});
            return query.ToList();
        }

        public bool UpdateBody(Filter filter)
        {
            ValidationModel(filter);
            var statementSql = $"UPDATE {_tableName} SET Body = @Body WHERE Name = @Name";            
            return Connection.Execute(statementSql, new {Body = filter.Body, Name = filter.Name }) > 0;
        }
    }
}
