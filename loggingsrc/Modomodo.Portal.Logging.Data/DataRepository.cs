﻿using System.Data;
using Modomodo.Portal.Logging.Data.Interfaces;

namespace Modomodo.Portal.Logging.Data
{
    public class DataRepository : DapperConnection, IDataRepository
    {
        public DataRepository(IDbConnection connection) : base(connection)
        {
        }
    }
}