﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Modomodo.Portal.Logging.Data.Tests
{
    [TestClass]
    public class DapperConnectionTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewDapperConnection_With_Connection_Null_Should_Return_ArgumentNullException()
        {
            var dapperConnection = new DapperConnection(null);
        }
    }
}
