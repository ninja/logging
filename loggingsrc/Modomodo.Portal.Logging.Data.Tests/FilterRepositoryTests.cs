﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.InternalUtilityTests;
using Modomodo.Portal.Logging.Model.Models;

namespace Modomodo.Portal.Logging.Data.Tests
{
    [TestClass]
    public class FilterRepositoryTests
    {
        private readonly DapperConnection _dapperConnection;
        private readonly FilterRepository _filterRepository;
        private readonly string _filterName;
        private Filter _filter;

        public FilterRepositoryTests()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["PortalLoggingConnection"];
            _dapperConnection = new DapperConnection(new SqlConnection(connectionString.ConnectionString));
            _filterName = GenerateFilterName();
            _filterRepository = new FilterRepository(_dapperConnection.Connection);
            InsertFilter();
        }

        [TestCleanup]
        public void CleanUp()
        {
            _filterRepository.Delete(_filter);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Delete_With_Filter_Null_Should_Return_ArgumentNullException()
        {
            _filterRepository.Delete(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Insert_With_Filter_Null_Should_Return_ArgumentNullException()
        {
            _filterRepository.Insert(null);
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void Insert_With_Wrong_ApplicationName_Should_Return_SqlException()
        {
            var filter = new Filter("name", "body", "wrongapp");
            _filterRepository.Insert(filter);
        }

        [TestMethod]
        [ExpectedException( typeof(ArgumentNullException))]
        public void WhereByFilterName_With_Name_Empty_Should_Return_ArgumentNullException()
        {
            _filterRepository.WhereByFilterName(string.Empty);
        }

        [TestMethod]
        public void WhereByFilterName_Check_Filter()
        {            
            var filter = _filterRepository.WhereByFilterName(_filterName);
            
            Assert.IsNotNull( filter);
            Assert.IsTrue( filter.Name == _filterName);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void WhereByApplicationName_With_ApplicationName_Empty_Should_Return_ArgumentNullException()
        {
            _filterRepository.WhereByApplicationName(string.Empty);
        }

        [TestMethod]
        public void WhereByApplicationName_Check_Filters()
        {            
            var filters = _filterRepository.WhereByApplicationName(Constants.ApplicationName);

            Assert.IsTrue( filters.Any());
        }

        [TestMethod]
        public void UpdateBody_Check_Filter()
        {
            const string modify = "newbody";
            var result = _filterRepository.UpdateBody(new Filter(_filterName, modify, Constants.ApplicationName));

            Assert.IsTrue( result);

            var filter = _filterRepository.WhereByFilterName(_filterName);

            Assert.IsTrue( filter.Body == modify);
        }

        private void InsertFilter()
        {
            _filter = new Filter(_filterName, "body", Constants.ApplicationName);
            _filterRepository.Insert(_filter);
        }

        private static string GenerateFilterName()
        {
            return "name" + Guid.NewGuid();
        }
    }
}