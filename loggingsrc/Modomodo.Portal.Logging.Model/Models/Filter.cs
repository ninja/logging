﻿using System;
using Modomodo.Portal.Logging.Model.Interfaces;

namespace Modomodo.Portal.Logging.Model.Models
{
    public class Filter : IDomainEntity
    {
        private readonly string _name;
        private readonly string _body;
        //TODO: da rimuovere appena si integra la classe Application
        private readonly string _applicationName;

        public Filter(string name, string body, string applicationName)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name));

            if (string.IsNullOrWhiteSpace(body))
                throw new ArgumentNullException(nameof(body));

            if( string.IsNullOrWhiteSpace(applicationName))
                throw new ArgumentNullException(applicationName);

            _name = name;
            _body = body;
            _applicationName = applicationName;
        }

        public string Name => _name;
        public string Body => _body;
        public string ApplicationName => _applicationName;
    }
}
