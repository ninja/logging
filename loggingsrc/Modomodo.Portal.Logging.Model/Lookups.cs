﻿using System;

namespace Modomodo.Portal.Logging.Model
{
    public static class LookupTags
    {
        public static string TagTest => "test";
        public static string TagProd => "prod";
    }

    public static class LookupElasticIndex
    {
        public static string IndexNameDev => "mmlogdevelopment";
        public static string IndexNameProd => "mmlog";

        public static string GetIndexNameByApplicationTag( string applicationTag)
        {
            var appTag = applicationTag.Trim().ToLower();
            if( appTag == LookupTags.TagTest)
                return IndexNameDev;
            if( appTag == LookupTags.TagProd)          
                return IndexNameProd;

            throw new Exception("tag non riconosciuto");
        }
    }
}
