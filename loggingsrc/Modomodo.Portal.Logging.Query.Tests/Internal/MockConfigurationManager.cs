﻿using System;
using System.Collections.Specialized;
using Modomodo.Platform.Stack.Interfaces;

namespace Modomodo.Portal.Logging.Query.Tests.Internal
{
    internal class MockConfigurationManager : IConfigurationManager
    {
        public NameValueCollection AppSettings
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string ConnectionStrings(string name)
        {
            return "elastic://127.0.0.1:9200/indexname";
        }

        public T GetSection<T>(string sectionName)
        {
            throw new NotImplementedException();
        }

        public string GetValueByKey(string appSettingKey)
        {
            throw new NotImplementedException();
        }
    }
}
