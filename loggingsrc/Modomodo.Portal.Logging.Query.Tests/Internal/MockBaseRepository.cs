﻿using Modomodo.Portal.Logging.Query.Elastic.Data;
using Modomodo.Portal.Logging.Query.Elastic.Models;

namespace Modomodo.Portal.Logging.Query.Tests.Internal
{
    internal class MockBaseRepository : BaseRepository<WebMessageTemplate>
    {
    }
}
