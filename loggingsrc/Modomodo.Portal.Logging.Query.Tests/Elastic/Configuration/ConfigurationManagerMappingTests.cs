﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Query.Elastic.Configuration;
using Modomodo.Portal.Logging.Query.Tests.Internal;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Configuration
{
    [TestClass]
    public class ConfigurationManagerMappingTests
    {
        [TestMethod]
        public void Call_ConfigurationScanner_To_Type_Without_ConfigurationMapping_Should_Return_Null()
        {
            var configurationMapping = ConfigurationManagerMapping.GetInstance().GetConfigurationMapping<FakeWebMessageTemplate>();
            Assert.IsNull( configurationMapping);
        }

        [TestMethod]
        public void ListMapping_Check_Result()
        {
            var configurationManagerMapping = ConfigurationManagerMapping.GetInstance();
            Assert.IsTrue( configurationManagerMapping.ListMapping().Count > 0);
        }
    }
}
