﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using Modomodo.Portal.Logging.Query.Elastic.Models.Request;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Models.Request
{
    [TestClass]
    public class SortOptionTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateSort_With_FieldName_Empty_Should_Return_ArgumentNullException()
        {
            SortOption<WebMessageTemplate>.CreateSort(x => x, true);
        }
    }
}
