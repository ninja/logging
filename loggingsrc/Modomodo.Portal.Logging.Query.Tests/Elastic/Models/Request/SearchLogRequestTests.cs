﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.InternalUtilityTests;
using Modomodo.Portal.Logging.Query.Elastic.Models.Request;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Models.Request
{
    [TestClass]
    public class SearchLogRequestTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SearchLogRequest_With_IndexName_Empty_Should_Return_ArgumentNullException()
        {
            SearchLogRequest.NewSearch(string.Empty, Constants.ApplicationName);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SearchLogRequest_With_DocumentTypeName_Empty_Should_Return_ArgumentNullException()
        {
            SearchLogRequest.NewSearch(Constants.IndexName, string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetFilterRaw_With_FilterRaw_Empty_Should_Return_ArgumentNullException()
        {
            var searchlogRequest = SearchLogRequest.NewSearch(Constants.IndexName, Constants.ApplicationName);
            searchlogRequest.SetFilterRaw(string.Empty);
        }

        [TestMethod]
        public void SetSortOption_Check_SortOptions()
        {
            const string fieldName = "fields.messagetemplate.CodeResult";

            var searchlogRequest = SearchLogRequest.NewSearch(Constants.IndexName, Constants.ApplicationName).SetSortOption(x => x.fields.messagetemplate.CodeResult, true);
            var sortOptions = searchlogRequest.SortOptions;

            Assert.IsTrue( sortOptions.Count == 1);
            Assert.IsTrue( sortOptions.FirstOrDefault(x => x.FieldName == fieldName).Ascending);
        }
    }
}
