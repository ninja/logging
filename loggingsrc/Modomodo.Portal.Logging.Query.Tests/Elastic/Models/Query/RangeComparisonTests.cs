﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Models.Query
{
    [TestClass]
    public class RangeComparisonTests
    {
        [TestMethod]
        public void GreaterThanOrEqual_Should_Return_gte()
        {
            var result = RangeComparison.GreaterThanOrEqual;
            Assert.IsTrue( result.Value == "gte");
        }

        [TestMethod]
        public void LessThanOrEqual_Should_Return_lte()
        {
            var result = RangeComparison.LessThanOrEqual;
            Assert.IsTrue( result.Value == "lte");
        }

        [TestMethod]
        public void FromSymbolToRangeComparison_Check_RangeComparison()
        {
            const string symbol1 = ">";
            const string symbol2 = "<";
            const string symbol3 = ">=";
            const string symbol4 = "<=";

            Assert.IsTrue( RangeComparison.FromSymbolToRangeComparison( symbol1).Value == RangeComparison.GreaterThan.Value);
            Assert.IsTrue(RangeComparison.FromSymbolToRangeComparison(symbol2).Value == RangeComparison.LessThan.Value);
            Assert.IsTrue(RangeComparison.FromSymbolToRangeComparison(symbol3).Value == RangeComparison.GreaterThanOrEqual.Value);
            Assert.IsTrue(RangeComparison.FromSymbolToRangeComparison(symbol4).Value == RangeComparison.LessThanOrEqual.Value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FromSymbolToRangeComparison_With_Empty_Symbol_Should_Return_ArgumentNullException()
        {
            RangeComparison.FromSymbolToRangeComparison(string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void FromSymbolToRangeComparison_With_Wrong_Symbol_Should_return_Exception()
        {
            RangeComparison.FromSymbolToRangeComparison("!");
        }
    }
}