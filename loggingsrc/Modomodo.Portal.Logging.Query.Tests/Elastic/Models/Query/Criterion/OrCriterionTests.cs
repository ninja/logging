﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Models.Query.Criterion
{
    [TestClass]
    public class OrCriterionTests
    {
        [TestMethod]
        public void OrCriterion_Check_StatementElastic()
        {
            const string result = @"""bool"":{""should"":[{""term"":{""price"":""20""}},{""term"":{""price"":""30""}}]}";

            var orCriterion = new OrCriterion(new TermCriterion( ElasticProperty.Create<WebMessageTemplate>( "price"), ElasticObject.Create( "20")),
                new TermCriterion( ElasticProperty.Create<WebMessageTemplate>( "price"), ElasticObject.Create("30")));
            var query = orCriterion.ToQuery();

            Assert.IsTrue(query == result);
        }
    }
}