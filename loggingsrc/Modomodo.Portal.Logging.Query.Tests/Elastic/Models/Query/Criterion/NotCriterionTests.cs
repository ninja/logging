﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Models.Query.Criterion
{
    [TestClass]
    public class NotCriterionTests
    {
        [TestMethod]
        public void NotCriterion_Check_StatementElastic()
        {
            const string result = @"""bool"":{""must_not"":[{""term"":{""price"":""20""}}]}";

            var notCriterion = new NotCriterion(new TermCriterion(ElasticProperty.Create<WebMessageTemplate>( "price"), ElasticObject.Create( "20")));
            var query = notCriterion.ToQuery();

            Assert.IsTrue(query == result);
        }
    }
}