﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Models.Query.Criterion
{
    [TestClass]
    public class MultipleExactTermCriterionTests
    {
        private readonly MultipleExactTermCriterion _multipleExactTermCriterion;

        public MultipleExactTermCriterionTests()
        {
            _multipleExactTermCriterion = new MultipleExactTermCriterion(
                ElasticProperty.Create<WebMessageTemplate>("fields.messagetemplate.Exception.HResult"),
                new List<ElasticObject> { ElasticObject.Create("-100"), ElasticObject.Create("100") });
        }

        [TestMethod]
        public void MultipleExactTermCriterion_Check_StatementElastic()
        {
            const string result = @"""terms"":{""fields.messagetemplate.Exception.HResult"":[""-100"",""100""]}";
            var query = _multipleExactTermCriterion.ToQuery();

            Assert.IsTrue(query == result);
        }

        [TestMethod]
        public void MultipleExactTermCriterion_Check_Values()
        {
            var values = _multipleExactTermCriterion.Values;
            Assert.IsNotNull( values);
            Assert.IsTrue( values.Count > 0);
        }
    }
}