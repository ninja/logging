﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Models.Query.Criterion
{
    [TestClass]
    public class MatchCriterionTests
    {
        [TestMethod]
        public void MatchCriterion_Check_StatementElastic()
        {
            const string result = @"""query"":{""match"":{""productID"":""abc""}}";
            var matchCriterion = new MatchCriterion( ElasticProperty.Create<WebMessageTemplate>( "productID"), ElasticObject.Create( "abc"));
            var query = matchCriterion.ToQuery();

            Assert.IsTrue(query == result);
        }
    }
}