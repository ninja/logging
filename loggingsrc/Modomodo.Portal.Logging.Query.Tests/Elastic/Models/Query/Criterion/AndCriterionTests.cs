﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Models.Query.Criterion
{
    [TestClass]
    public class AndCriterionTests
    {
        [TestMethod]
        public void AndCriterion_Check_StatementElastic()
        {
            const string result = @"""bool"":{""must"":[{""term"":{""price"":""20""}},{""term"":{""productID"":""abc""}}]}";

            var andCriterion = new AndCriterion(
                new TermCriterion( ElasticProperty.Create<WebMessageTemplate>("price"),ElasticObject.Create("20")), 
                new TermCriterion( ElasticProperty.Create<WebMessageTemplate>("productID"), ElasticObject.Create( "abc")));
            var query = andCriterion.ToQuery();

            Assert.IsTrue(query == result);
        }
    }
}