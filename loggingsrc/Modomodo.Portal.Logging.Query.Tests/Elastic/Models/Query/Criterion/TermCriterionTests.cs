﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Models.Query.Criterion
{
    [TestClass]
    public class TermCriterionTests
    {
        [TestMethod]
        public void TermCriterion_Check_StatementElastic()
        {
            const string result = @"""term"":{""price"":""20""}";

            var termCriterion = new TermCriterion(ElasticProperty.Create<WebMessageTemplate>( "price"), ElasticObject.Create( "20"));
            var query = termCriterion.ToQuery();

            Assert.IsTrue(query == result);
        }
    }
}