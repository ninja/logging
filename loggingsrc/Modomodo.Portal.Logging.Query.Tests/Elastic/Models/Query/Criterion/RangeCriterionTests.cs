﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Models.Query.Criterion
{
    [TestClass]
    public class RangeCriterionTests
    {
        private readonly RangeCriterion _rangeCriterion;
        public RangeCriterionTests()
        {
            _rangeCriterion = new RangeCriterion( ElasticProperty.Create<WebMessageTemplate>("level"),
                new RangeSpecification(RangeComparison.GreaterThan, ElasticObject.Create("20")),
                new RangeSpecification(RangeComparison.LessThan, ElasticObject.Create("40")));
        }

        [TestMethod]
        public void RangeCriterion_Check_StatementElastic()
        {
            const string result = @"""range"":{""level"":{""gt"":""20"",""lt"":""40""}}";
            var query = _rangeCriterion.ToQuery();

            Assert.IsTrue(query == result);
        }

        [TestMethod]
        public void RangeCriterion_With_Only_One_Specification_Check_StatemenetElastic()
        {
            const string result = @"""range"":{""level"":{""gt"":""20""}}";
            var unboundedRangeCriterion = new RangeCriterion(ElasticProperty.Create<WebMessageTemplate>("level"), 
                new RangeSpecification(RangeComparison.GreaterThan, ElasticObject.Create("20")));
            var query = unboundedRangeCriterion.ToQuery();

            Assert.IsTrue(query == result);
        }

        [TestMethod]
        public void RangeCriterion_Check_Field()
        {
            var field = _rangeCriterion.Field;
            Assert.IsTrue( field.FieldName == "level");
        }
    }
}