﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Models.Search
{
    [TestClass]
    public class ElasticTypeAnalyzerTest
    {
        [TestMethod]
        public void ElasticTypeAnalyzer_Check_Whitespace()
        {
            var elasticTypeAnalyzer = (ElasticTypeAnalyzer)ElasticTypeAnalyzer.Whitespace;

            Assert.IsFalse(elasticTypeAnalyzer.LowerCases);
        }
    }
}
