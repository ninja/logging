﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Models.Search
{
    [TestClass]
    public class ElasticPropertyTests
    {
        [TestMethod]
        public void Create_Check_Type_ConfigurationMapping()
        {
            var elasticProperty = ElasticProperty.Create<WebMessageTemplate>("level");
            Assert.IsInstanceOfType(elasticProperty.Analyzer, ElasticTypeAnalyzer.None.GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Create_With_FieldName_Empty_Should_ReturnArgumentNullException()
        {
            ElasticProperty.Create<WebMessageTemplate>(string.Empty);
        }
    }
}