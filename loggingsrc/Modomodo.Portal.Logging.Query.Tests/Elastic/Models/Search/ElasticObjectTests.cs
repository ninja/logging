﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Models.Search
{
    [TestClass]
    public class ElasticObjectTests
    {
        [TestMethod]
        public void Create_Check_OriginalValue()
        {
            const string originalvalue = "original value";
            var elasticObject = ElasticObject.Create(originalvalue);

            Assert.IsTrue( elasticObject.OriginalValue == originalvalue);
        }

        [TestMethod]
        public void GetValue_With_ElasticTypeAnalyzer_Null_Should_Return_Original_Value()
        {
            const string originalValue = "VaLue";

            var elasticObject = ElasticObject.Create(originalValue);
            var result = elasticObject.TrasformationValue(null);

            Assert.IsTrue( result == originalValue);
        }

        [TestMethod]
        public void GetValue_With_StandardTypeAnalyzer_Should_Return_ToLower_Value()
        {
            const string originalValue = "VaLue";

            var elasticObject = ElasticObject.Create(originalValue);
            var result = elasticObject.TrasformationValue(ElasticTypeAnalyzer.Standard);
            Assert.AreEqual( result, originalValue.ToLower());
        }
    }
}
