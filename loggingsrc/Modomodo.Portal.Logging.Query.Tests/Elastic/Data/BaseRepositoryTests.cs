﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.InternalUtilityTests;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using Modomodo.Portal.Logging.Query.Elastic.Models.Request;
using Modomodo.Portal.Logging.Query.Tests.Internal;
using Nest;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Data
{
    [TestClass]
    public class BaseRepositoryTests
    {
        [TestMethod]
        public void GetBaseSearchDescriptor_Check_SortAscending()
        {
            var sort = GetSort(true);

            Assert.IsTrue(sort.Count == 1);
            Assert.IsTrue(sort.FirstOrDefault().Key.Name == "level");
            Assert.IsTrue(sort.FirstOrDefault().Value.Order == SortOrder.Ascending);
        }

        [TestMethod]
        public void GetBaseSearchDescriptor_Check_SortDescending()
        {
            var sort = GetSort(false);

            Assert.IsTrue( sort.Count == 1);
            Assert.IsTrue(sort.FirstOrDefault().Value.Order == SortOrder.Descending);
        }

        private IList<KeyValuePair<PropertyPathMarker, ISort>> GetSort( bool ascending)
        {
            var sortAscending = SortOption<WebMessageTemplate>.CreateSort(x => x.level, ascending);

            var searchRequest =
                SearchLogRequest.NewSearch(Constants.IndexName, Constants.ApplicationName).SetSortOptions(new List<SortOption<WebMessageTemplate>> { sortAscending });

            var mockBaseRepository = new MockBaseRepository();
            var searchDescriptor = mockBaseRepository.GetBaseSearchDescriptor(searchRequest);
            var sortResult = ((ISearchRequest)searchDescriptor).Sort;
            return sortResult;
        }
    }
}