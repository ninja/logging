﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Platform.Stack.Interfaces;
using Modomodo.Portal.Logging.Query.Elastic.Data;
using Modomodo.Portal.Logging.Query.Tests.Internal;
using NSubstitute;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Data
{
    [TestClass]
    public class ConnectionStringTests
    {
        [TestMethod]
        [ExpectedException(typeof(UriFormatException))]
        public void ExtractServer_With_Wrong_EndPoint_Should_Return_UriFormatException()
        {
            var configurationManager = Substitute.For<IConfigurationManager>();
            configurationManager.ConnectionStrings("elasticsearch").Returns("elastic://fake&url");
            new ConnectionString(configurationManager);
        }

        [TestMethod]
        [ExpectedException(typeof(ConfigurationErrorsException))]
        public void ExtractServer_With_Zero_End_Point_Should_Return_Empty_ListServer()
        {
            var configurationManager = Substitute.For<IConfigurationManager>();
            configurationManager.ConnectionStrings("elasticsearch").Returns("elastic://");
            new ConnectionString(configurationManager);
        }

        [TestMethod]
        [ExpectedException(typeof(ConfigurationErrorsException))]
        public void ConnectionString_With_ConnectionName_Empty_Should_Return_ConfigurationErrorsException()
        {
            var configurationManager = Substitute.For<IConfigurationManager>();
            configurationManager.ConnectionStrings("fake name connection").Returns(string.Empty);
            new ConnectionString(configurationManager);
        }

        [TestMethod]
        [ExpectedException(typeof(ConfigurationErrorsException))]
        public void Parse_With_Wrong_Format_ElasticConnectionString_Should_Return_ConfigurationErrorsException()
        {
            var configurationManager = Substitute.For<IConfigurationManager>();
            configurationManager.ConnectionStrings("elasticsearch").Returns("error parse");
            new ConnectionString(configurationManager);
        }

        [TestMethod]
        public void ConnectionString_Check_Connection()
        {
            var configurationManager = new ConfigurationManagerWrapper();
            var connection = new ConnectionString(configurationManager);

            Assert.IsNotNull(connection.Server);
            Assert.IsTrue(connection.Server.Count > 0);
        }

        [TestMethod]
        public void ConnectionString_Check_Server()
        {
            var mockConfigurationManager = new MockConfigurationManager();
            var connection = new ConnectionString(mockConfigurationManager);

            Assert.IsNotNull(connection.Server);
            Assert.IsTrue(connection.Server.Count == 1);
        }
    }
}