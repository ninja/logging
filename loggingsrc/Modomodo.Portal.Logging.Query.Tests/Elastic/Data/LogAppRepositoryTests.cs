﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.InternalUtilityTests;
using Modomodo.Portal.Logging.Query.Elastic.Data;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion;
using Modomodo.Portal.Logging.Query.Elastic.Models.Request;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;
using Modomodo.Portal.Logging.Query.Elastic.Models.Visitor;
using Nest;

namespace Modomodo.Portal.Logging.Query.Tests.Elastic.Data
{
    [TestClass]
    public class LogAppRepositoryTests
    {
        private readonly LogAppRepository _logRepository;

        public LogAppRepositoryTests()
        {
            _logRepository = new LogAppRepository();
        }

        [TestMethod]
        public void GetBaseSearchDescriptor_Check_Default_Parameters_Of_SearchDescriptor()
        {
            var searchRequest =
                SearchLogRequest.NewSearch(Constants.IndexName, Constants.ApplicationName).SetDefaultSortOption();
            var searchDescriptor = _logRepository.GetBaseSearchDescriptor(searchRequest);

            var sort = ((ISearchRequest) searchDescriptor).Sort;

            Assert.IsTrue(((ISearchRequest)searchDescriptor).Indices.FirstOrDefault()?.Name == searchRequest.IndexName);
            Assert.IsTrue(((ISearchRequest) searchDescriptor).Types.FirstOrDefault()?.Name == searchRequest.DocumentTypeName);
            Assert.IsTrue(((ISearchRequest)searchDescriptor).Size == searchRequest.MaxSizeReturnDocuments);
            Assert.IsTrue(sort.FirstOrDefault().Key.Name == "fields.messagetemplate.Date");
            Assert.IsTrue(sort.FirstOrDefault().Value.Order.Value == SortOrder.Descending);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetBaseSearchDescriptor_With_SortOptions_Null_Should_Return_ArgumentNullException()
        {
            SearchLogRequest.NewSearch(Constants.IndexName, Constants.ApplicationName).SetSortOptions(null);
        }

        [TestMethod]
        public void BaseRepository_Check_Client_Should_Be_Not_Null()
        {
            var client = _logRepository.Client;
            Assert.IsNotNull(client);
        }

        [TestMethod]
        public void BaseRepository_Check_Ping_Client()
        {
            var ping = _logRepository.Client.Ping();
            var result = ping.IsValid;
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CountDocuments_Check_Value()
        {
            var count = _logRepository.CountDocuments(
                SearchLogRequest.NewSearch(Constants.IndexName, Constants.ApplicationName));
            Assert.IsTrue(count > 0);
        }

        [TestMethod]
        public void GetLastLogs_Check_List_Logs()
        {
            const int maxDocument = 5;
            var result = _logRepository.GetLastLogs(
                SearchLogRequest.NewSearch(Constants.IndexName, Constants.ApplicationName)
                .AddSize(maxDocument)
                .SetDefaultSortOption());

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count() == maxDocument);
        }

        [TestMethod]
        public void SearchLogs_Check_List_Logs()
        {
            const string level = "Error";
            const string message = "eon_exception";

            var elasticSearchQueryVisitor = new ElasticSearchQueryVisitor();
            var chainCriteria = new ChainSearchCriteria();
            chainCriteria.AddCriterion(
                new AndCriterion(
                    new TermCriterion(ElasticProperty.Create<WebMessageTemplate>("level"), ElasticObject.Create(level)),
                    new MatchCriterion( ElasticProperty.Create<WebMessageTemplate>("fields.messagetemplate.Message"), ElasticObject.Create(message))
                    ));
            chainCriteria.Accept(elasticSearchQueryVisitor);
            var queryRaw = elasticSearchQueryVisitor.Filter;

            var searchLogRequest = SearchLogRequest.NewSearch(Constants.IndexName, Constants.ApplicationName)
                .AddSize(5)
                .SetDefaultSortOption()
                .SetFilterRaw(queryRaw);
            var documents = _logRepository.SearchLogs(searchLogRequest);

            Assert.IsTrue(documents.Any());
            foreach( var result in documents)
            {
                Assert.IsTrue(result.level == level);
                Assert.IsTrue(result.fields.messagetemplate.Message.Contains(message));
            }
        }
    }
}