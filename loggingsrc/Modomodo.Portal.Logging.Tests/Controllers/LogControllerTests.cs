﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Controllers;
using Modomodo.Portal.Logging.Tests.Internal;

namespace Modomodo.Portal.Logging.Tests.Controllers
{
    [TestClass]
    public class LogControllerTests
    {
        public LogControllerTests()
        {
            MapperConfiguration.Configuration();
        }

        [TestMethod]
        public void GetLastLogs_Should_Return_Last_Logs()
        {
            var mockBaseRepository = new MockLogAppRepository();
            var controller = new LogController(mockBaseRepository);
            var result = controller.GetLastLogs(InternalUtilityTests.Constants.IndexName, InternalUtilityTests.Constants.ApplicationName);

            Assert.IsTrue(result.Content.Count() == MockLogAppRepository.GetTestWebMessageTemplate().Count);
        }
    }
}