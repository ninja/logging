﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Controllers;
using System.Configuration;

namespace Modomodo.Portal.Logging.Tests.Controllers
{
    [TestClass]
    public class BaseControllerTests
    {
        [TestMethod]
        public void MaxDocuments_Should_Return_The_Value_Of_Key()
        {
            var baseController = new BaseController();
            var result = baseController.MaxDocuments;

            Assert.IsTrue(result == int.Parse(ConfigurationManager.AppSettings["MaxDocuments"]));
        }
    }
}
