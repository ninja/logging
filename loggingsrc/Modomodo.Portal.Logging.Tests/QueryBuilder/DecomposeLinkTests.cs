﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Models.Inbound;
using Modomodo.Portal.Logging.Models.Outbound;
using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Query;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion;
using Modomodo.Portal.Logging.QueryBuilder;
using NSubstitute;
using NSubstitute.Core;

namespace Modomodo.Portal.Logging.Tests.QueryBuilder
{
    [TestClass]
    public class DecomposeLinkTests
    {
        private readonly ICriteria _chainSearchCriteria;

        public DecomposeLinkTests()
        {
            _chainSearchCriteria = Substitute.For<ICriteria>();
        }

        [TestMethod]
        public void DecomposeLink_Check_ElasticSearchFilter()
        {
            var chain = Fakes.GetFakeChain();
            var decomposeLink = new DecomposeLink(new ChainSearchCriteria(), chain);
            var filter = decomposeLink.GetFilterByLinks();

            Assert.IsTrue( filter.Length > 0);
        }

        [TestMethod]
        public void DecomposeLink_With_Term_Criterion_Check_A_Call()
        {
            var chain = new Chain
            {
                Link =
                    new SimpleLink ("level")
                    {
                        Name = ConfigurationTermCriterion.Name,
                        Values = new List<string> {"debug"}
                    }
            };

            var calls = GetCallsByGetFilterByLinks(chain);

            Assert.IsTrue( calls.Any());
            var countCall = CountCallByTypeCriterion(calls, typeof (TermCriterion));
            Assert.IsTrue( countCall == 1);
        }

        [TestMethod]
        public void DecomposeLink_With_MultipleExactTerm_Criterion_Check_A_Call()
        {
            var chain = new Chain
            {
                Link =
                    new SimpleLink("level")
                    {
                        Name = ConfigurationMultipleExactTermCriterion.Name,
                        Values = new List<string> {"debug", "error"}
                    }
            };
            var calls = GetCallsByGetFilterByLinks(chain);

            var countCall = CountCallByTypeCriterion(calls, typeof (MultipleExactTermCriterion));
            Assert.IsTrue( countCall == 1);
        }

        [TestMethod]
        public void DecomposeLink_With_Match_Criterion_Check_A_call()
        {
            var chain = new Chain
            {
                Link =
                    new SimpleLink("level")
                    {
                        Name = ConfigurationMatchCriterion.Name,
                        Values = new List<string> {"debug"}
                    }
            };

            var calls = GetCallsByGetFilterByLinks(chain);
            var countCall = CountCallByTypeCriterion(calls, typeof (MatchCriterion));

            Assert.IsTrue( countCall == 1);
        }

        [TestMethod]
        public void DecomposeLink_With_Range_Criterion_Specifying_One_Filter_Check_A_Call()
        {
            var rangeSimpleLink = new RangeLink ("level") { Name = ConfigurationRangeCriterion.Name, First = new Range { Symbol = ">", Value = "10" } };
            var chain = new Chain { Link = rangeSimpleLink, Name = "test" };

            var calls = GetCallsByGetFilterByLinks(chain);

            Assert.IsTrue(calls.Any());
            var countCall = CountCallByTypeCriterion(calls, typeof(RangeCriterion));
            Assert.IsTrue(countCall == 1);
        }

        [TestMethod]
        public void DecomposeLink_With_Range_Criterion_Specifying_Two_Filter_Check_A_Call()
        {
            var rangeSimpleLink = new RangeLink("level") { Name = ConfigurationRangeCriterion.Name, First = new Range { Symbol = ">", Value = "10" }, Second = new Range { Symbol = "<", Value = "20"} };
            var chain = new Chain { Link = rangeSimpleLink, Name = "test" };

            var calls = GetCallsByGetFilterByLinks(chain);

            Assert.IsTrue(calls.Any());
            var countCall = CountCallByTypeCriterion(calls, typeof(RangeCriterion));
            Assert.IsTrue(countCall == 1);
        }

        [TestMethod]
        public void DecomposeLink_With_Not_Criterion_Check_A_Call()
        {
            var simpleLink = new SimpleLink("level")
            {
                Name = ConfigurationTermCriterion.Name,
                Values = new List<string> {"debug"}
            };

            var notComplexLink = new ComplexLink { Name = ConfigurationNotCriterion.Name };
            notComplexLink.Links.Add( simpleLink);
            var chain = new Chain {Link = notComplexLink};
            
            var calls = GetCallsByGetFilterByLinks(chain);

            Assert.IsTrue( calls.Any());
            var countCall = CountCallByTypeCriterion(calls, typeof (NotCriterion));
            Assert.IsTrue( countCall == 1);
        }

        [TestMethod]
        public void DecomposeLink_With_And_Criterion_Check_A_Call()
        {
            var simpleLink1 = new SimpleLink("level")
            {
                Name = ConfigurationTermCriterion.Name,
                Values = new List<string> { "debug" }
            };

            var simpleLink2 = new SimpleLink("level")
            {
                Name = ConfigurationMatchCriterion.Name,
                Values = new List<string> { "error" }
            };

            var andComplexLink = new ComplexLink { Name = ConfigurationAndCriterion.Name };
            andComplexLink.Links.Add(simpleLink1);
            andComplexLink.Links.Add(simpleLink2);
            var chain = new Chain { Link = andComplexLink };

            var calls = GetCallsByGetFilterByLinks(chain);

            Assert.IsTrue(calls.Any());
            var countCall = CountCallByTypeCriterion(calls, typeof(AndCriterion));
            Assert.IsTrue(countCall == 1);
        }

        [TestMethod]
        public void DecomposeLink_With_Or_Criterion_Check_A_Call()
        {
            var simpleLink1 = new SimpleLink("level")
            {
                Name = ConfigurationTermCriterion.Name,
                Values = new List<string> { "debug" }
            };

            var simpleLink2 = new SimpleLink("level")
            {
                Name = ConfigurationMatchCriterion.Name,
                Values = new List<string> { "error" }
            };

            var orComplexLink = new ComplexLink { Name = ConfigurationOrCriterion.Name };
            orComplexLink.Links.Add(simpleLink1);
            orComplexLink.Links.Add(simpleLink2);
            var chain = new Chain { Link = orComplexLink };

            var calls = GetCallsByGetFilterByLinks(chain);

            Assert.IsTrue(calls.Any());
            var countCall = CountCallByTypeCriterion(calls, typeof(OrCriterion));
            Assert.IsTrue(countCall == 1);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void DecomposeLink_Validate_SimpleLink_With_Wrong_Name_Should_Return_Exception()
        {
            var simpleLink = new SimpleLink("level") { Name = "wrong", Values = new List<string> { "value1" } };
            var chain = new Chain { Link = simpleLink };
            var decomposeLink = new DecomposeLink( new ChainSearchCriteria(), chain);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void DecomposeLink_Validate_RangeLink_With_Wrong_Name_Should_Return_Exception()
        {
            var rangeLink = new RangeLink("level") {Name = "wrong", First = new Range {Symbol = ">", Value = "10"}};
            var chain = new Chain {Link = rangeLink};
            var decomposeLink = new DecomposeLink(new ChainSearchCriteria(), chain);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DecomposeLink_Validate_RangeLink_With_FirstSpecification_Null_Should_Return_ArgumentNullException()
        {
            var rangeLink = new RangeLink("level") { Name = ConfigurationRangeCriterion.Name };
            var chain = new Chain { Link = rangeLink };
            var decomposeLink = new DecomposeLink(new ChainSearchCriteria(), chain);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void DecomposeLink_Validate_ComplexLink_With_Wrong_Name_Should_Return_Exception()
        {
            var complexLink = new ComplexLink {Name = "wrong"};
            var chain = new Chain {Link = complexLink};
            var decomposeLink = new DecomposeLink( new ChainSearchCriteria(), chain);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void DecomposeLink_Validate_ComplexLink_Of_Type_AndCriterion_With_Less_Two_Criterion_Should_Return_Exception()
        {
            var complexLink = new ComplexLink {Name = ConfigurationAndCriterion.Name};
            complexLink.Links.Add(new SimpleLink("level")
            {
                Name = ConfigurationTermCriterion.Name,
                Values = new List<string> {"debug"}
            });
            var chain = new Chain {Link = complexLink};
            var decomposeLink = new DecomposeLink( new ChainSearchCriteria(), chain);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void DecomposeLink_Validate_CompleLink_Of_Type_NotCriterion_With_More_Two_Criterion_Should_Return_Exception()
        {
            var complexLink = new ComplexLink {Name = ConfigurationNotCriterion.Name};
            complexLink.Links.Add( new SimpleLink("level") { Name = ConfigurationTermCriterion.Name, Values = new List<string> {"debug"} } );
            complexLink.Links.Add( new SimpleLink( "date") {Name = ConfigurationTermCriterion.Name, Values = new List<string> { DateTime.Now.ToShortDateString()} } );
            var chain = new Chain {Link = complexLink};
            var decomposeLink = new DecomposeLink( new ChainSearchCriteria(), chain);
        }

        private List<ICall> GetCallsByGetFilterByLinks(Chain chain)
        {
            var decomposeLink = new DecomposeLink(_chainSearchCriteria, chain);
            decomposeLink.GetFilterByLinks();
            var calls = _chainSearchCriteria.Received().ReceivedCalls().ToList();
            return calls;
        }

        private static int CountCallByTypeCriterion(IEnumerable<ICall> calls, Type criterion)
        {
            return calls.Count(x => x.GetArguments().FirstOrDefault(a => a.GetType() == criterion) != null);
        }
    }
}
