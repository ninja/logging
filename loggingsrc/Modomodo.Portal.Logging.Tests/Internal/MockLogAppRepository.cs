﻿using System;
using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Data;
using Modomodo.Portal.Logging.Query.Elastic.Models.Request;
using System.Collections.Generic;
using Modomodo.Portal.Logging.Query.Elastic.Models;

namespace Modomodo.Portal.Logging.Tests.Internal
{
    internal class MockLogAppRepository : ILogRepository
    {
        public Nest.IElasticClient Client
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public long CountDocuments(SearchRequest<WebMessageTemplate> searchRequest)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<WebMessageTemplate> GetLastLogs(SearchRequest<WebMessageTemplate> searchRequest)
        {
            return GetTestWebMessageTemplate();
        }

        public static List<WebMessageTemplate> GetTestWebMessageTemplate()
        {
            var testwebMessageTemplate = new List<WebMessageTemplate>
            {
                new WebMessageTemplate {level = "Error"},
                new WebMessageTemplate {level = "Error"},
                new WebMessageTemplate {level = "Error"},
                new WebMessageTemplate {level = "Error"}
            };
            return testwebMessageTemplate;
        }

        public IEnumerable<WebMessageTemplate> SearchLogs(SearchRequest<WebMessageTemplate> searchRequest)
        {
            throw new NotImplementedException();
        }
    }
}