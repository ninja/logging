﻿using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Models.Inbound;
using Modomodo.Portal.Logging.Models.Outbound;

namespace Modomodo.Portal.Logging.Tests.Models.Outbound
{
    [TestClass]
    public class ConfigurationCriterionsTests
    {
        [TestMethod]
        public void Empty_Check_List()
        {
            var list = UtilityConfiguationCriterions.Empty();

            Assert.IsNotNull( list);
            Assert.IsTrue( list.GetType() == typeof(ReadOnlyCollection<string>));
            Assert.IsTrue( list.Count == 0);
        }

        [TestMethod]
        public void ConfigurationAndCriterion_Check_Properties()
        {
            Assert.IsTrue(ConfigurationAndCriterion.Name == "must");
            Assert.IsTrue(ConfigurationAndCriterion.Alias == "AND");
            Assert.IsTrue(ConfigurationAndCriterion.Operators.Count == 0);
            Assert.IsTrue(ConfigurationAndCriterion.PossibleJoints.Contains(ConfigurationAndCriterion.Name));
            Assert.IsTrue(ConfigurationAndCriterion.PossibleJoints.Contains(ConfigurationOrCriterion.Name));
            Assert.IsTrue(ConfigurationAndCriterion.PossibleJoints.Contains(ConfigurationNotCriterion.Name));
            Assert.IsTrue(ConfigurationAndCriterion.PossibleJoints.Contains(ConfigurationTermCriterion.Name));
            Assert.IsTrue(ConfigurationAndCriterion.PossibleJoints.Contains(ConfigurationMultipleExactTermCriterion.Name));
            Assert.IsTrue(ConfigurationAndCriterion.PossibleJoints.Contains(ConfigurationMatchCriterion.Name));
            Assert.IsTrue(ConfigurationAndCriterion.PossibleJoints.Contains(ConfigurationRangeCriterion.Name));
            Assert.IsTrue(ConfigurationAndCriterion.TypeLink == typeof(ComplexLink));
        }

        [TestMethod]
        public void ConfigurationOrCriterion_Check_Properties()
        {
            Assert.IsTrue(ConfigurationOrCriterion.Name == "should");
            Assert.IsTrue(ConfigurationOrCriterion.Alias == "OR");
            Assert.IsTrue(ConfigurationOrCriterion.Operators.Count == 0);
            Assert.IsTrue(ConfigurationOrCriterion.PossibleJoints.Contains(ConfigurationAndCriterion.Name));
            Assert.IsTrue(ConfigurationOrCriterion.PossibleJoints.Contains(ConfigurationOrCriterion.Name));
            Assert.IsTrue(ConfigurationOrCriterion.PossibleJoints.Contains(ConfigurationNotCriterion.Name));
            Assert.IsTrue(ConfigurationOrCriterion.PossibleJoints.Contains(ConfigurationTermCriterion.Name));
            Assert.IsTrue(ConfigurationOrCriterion.PossibleJoints.Contains(ConfigurationMatchCriterion.Name));
            Assert.IsTrue(ConfigurationOrCriterion.PossibleJoints.Contains(ConfigurationMultipleExactTermCriterion.Name));
            Assert.IsTrue(ConfigurationOrCriterion.PossibleJoints.Contains(ConfigurationRangeCriterion.Name));
            Assert.IsTrue(ConfigurationOrCriterion.TypeLink == typeof( ComplexLink));
        }

        [TestMethod]
        public void ConfigurationNotCriterion_Check_Properties()
        {
            Assert.IsTrue(ConfigurationNotCriterion.Name == "must_not");
            Assert.IsTrue(ConfigurationNotCriterion.Alias == "NOT");
            Assert.IsTrue(ConfigurationNotCriterion.Operators.Count == 0);
            Assert.IsTrue(ConfigurationNotCriterion.PossibleJoints.Contains(ConfigurationAndCriterion.Name));
            Assert.IsTrue(ConfigurationNotCriterion.PossibleJoints.Contains(ConfigurationOrCriterion.Name));
            Assert.IsTrue(ConfigurationNotCriterion.PossibleJoints.Contains(ConfigurationNotCriterion.Name));
            Assert.IsTrue(ConfigurationNotCriterion.PossibleJoints.Contains(ConfigurationTermCriterion.Name));
            Assert.IsTrue(ConfigurationNotCriterion.PossibleJoints.Contains(ConfigurationMatchCriterion.Name));
            Assert.IsTrue(ConfigurationNotCriterion.PossibleJoints.Contains(ConfigurationMultipleExactTermCriterion.Name));
            Assert.IsTrue(ConfigurationNotCriterion.PossibleJoints.Contains(ConfigurationRangeCriterion.Name));
            Assert.IsTrue(ConfigurationNotCriterion.TypeLink == typeof(ComplexLink));
        }

        [TestMethod]
        public void ConfigurationTermCriterion_Check_Properties()
        {
            Assert.IsTrue(ConfigurationTermCriterion.Name == "term");
            Assert.IsTrue(ConfigurationTermCriterion.Alias == "EQUAL");
            Assert.IsTrue(ConfigurationTermCriterion.Operators.Count == 1);
            Assert.IsTrue(ConfigurationTermCriterion.Operators.Contains("="));
            Assert.IsTrue(ConfigurationTermCriterion.PossibleJoints.Count == 0);
            Assert.IsTrue(ConfigurationTermCriterion.TypeLink == typeof( SimpleLink));
        }

        [TestMethod]
        public void ConfigurationMultipleExactTermCriterion_Check_properties()
        {
            Assert.IsTrue(ConfigurationMultipleExactTermCriterion.Name == "terms");
            Assert.IsTrue(ConfigurationMultipleExactTermCriterion.Alias == "IN");
            Assert.IsTrue(ConfigurationMultipleExactTermCriterion.Operators.Count == 1);
            Assert.IsTrue(ConfigurationMultipleExactTermCriterion.Operators.Contains("="));
            Assert.IsTrue(ConfigurationMultipleExactTermCriterion.PossibleJoints.Count == 0);
            Assert.IsTrue(ConfigurationMultipleExactTermCriterion.TypeLink == typeof(SimpleLink));
        }

        [TestMethod]
        public void ConfigurationMatchCriterion_Check_Properties()
        {
            Assert.IsTrue(ConfigurationMatchCriterion.Name =="match");
            Assert.IsTrue(ConfigurationMatchCriterion.Alias == "LIKE");
            Assert.IsTrue(ConfigurationMatchCriterion.Operators.Count == 0);
            Assert.IsTrue(ConfigurationMatchCriterion.PossibleJoints.Count == 0);
            Assert.IsTrue(ConfigurationMatchCriterion.TypeLink == typeof( SimpleLink));
        }

        [TestMethod]
        public void ConfigurationRangeCriterion_Check_Properties()
        {
            Assert.IsTrue(ConfigurationRangeCriterion.Name == "range");
            Assert.IsTrue(ConfigurationRangeCriterion.Alias == "RANGE");
            Assert.IsTrue(ConfigurationRangeCriterion.Operators.Count == 4);
            Assert.IsTrue(ConfigurationRangeCriterion.Operators.Contains(">"));
            Assert.IsTrue(ConfigurationRangeCriterion.Operators.Contains("<"));
            Assert.IsTrue(ConfigurationRangeCriterion.Operators.Contains(">="));
            Assert.IsTrue(ConfigurationRangeCriterion.Operators.Contains("<="));
            Assert.IsTrue(ConfigurationRangeCriterion.PossibleJoints.Count == 0);
            Assert.IsTrue(ConfigurationRangeCriterion.TypeLink == typeof (RangeLink));
        }
    }
}