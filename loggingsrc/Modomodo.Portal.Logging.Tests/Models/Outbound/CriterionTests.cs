﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Models.Inbound;
using Modomodo.Portal.Logging.Models.Outbound;

namespace Modomodo.Portal.Logging.Tests.Models.Outbound
{
    [TestClass]
    public class CriterionTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewCriterion_With_Name_Empty_Should_Return_ArgumentNullException()
        {
            new Criterion(string.Empty, "alias", typeof (ComplexLink));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewCriterion_With_Alias_Empty_Should_Return_ArgumentNullException()
        {
            new Criterion("name", string.Empty, typeof(ComplexLink));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewCriterion_With_Type_Null_Should_Return_ArgumentNullException()
        {
            new Criterion("name", "alias", null);
        }

        [TestMethod]
        public void NewCriterion_Check_Properties()
        {
            var criterion = new Criterion(ConfigurationTermCriterion.Name, ConfigurationTermCriterion.Alias,
                typeof (SimpleLink));

            Assert.IsTrue(criterion.Name == ConfigurationTermCriterion.Name);
            Assert.IsTrue(criterion.Alias == ConfigurationTermCriterion.Alias);
            Assert.IsTrue(criterion.AssemblyQualifiedName == typeof(SimpleLink).AssemblyQualifiedName);
            Assert.IsTrue(criterion.Operators.Count == 0);
            Assert.IsTrue(criterion.PossibleJoints.Count == 0);
        }

        [TestMethod]
        public void JointCriterionName_Check_List()
        {
            var names = new List<string> {ConfigurationTermCriterion.Name, ConfigurationAndCriterion.Name};
            var criterion = new Criterion( ConfigurationAndCriterion.Name, ConfigurationAndCriterion.Alias, typeof( ComplexLink));
            criterion.JointCriterionName(new ReadOnlyCollection<string>(names));

            Assert.IsTrue( criterion.PossibleJoints.Count == 2);
            Assert.IsTrue(criterion.PossibleJoints.Contains( ConfigurationTermCriterion.Name));
            Assert.IsTrue(criterion.PossibleJoints.Contains(ConfigurationAndCriterion.Name));
        }

        [TestMethod]
        public void SetOperators_Check_List()
        {
            var operators = new List<string> {"=", "!="};
            var criterion = new Criterion(ConfigurationAndCriterion.Name, ConfigurationAndCriterion.Alias, typeof(ComplexLink));
            criterion.SetOperators(operators);

            Assert.IsTrue(criterion.Operators.Count == 2);
            Assert.IsTrue(criterion.Operators.Contains("="));
            Assert.IsTrue(criterion.Operators.Contains("!="));
        }
    }
}
