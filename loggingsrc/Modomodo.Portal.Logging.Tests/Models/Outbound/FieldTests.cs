﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Models.Outbound;

namespace Modomodo.Portal.Logging.Tests.Models.Outbound
{
    [TestClass]
    public class FieldTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewField_With_Name_Empty_Should_Return_ArgumentNullException()
        {
            new Field(string.Empty, "alias", "type");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewField_With_Alias_Empty_Should_Return_ArgumentNullException()
        {
            new Field("name", string.Empty, "type");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewField_With_Type_Empty_Should_Return_ArgumentNullException()
        {
            new Field("name", "alias", string.Empty);
        }

        [TestMethod]
        public void NewField_Check_Properties()
        {
            var field = new Field("name", "alias", "type");

            Assert.IsTrue( field.Name == "name");
            Assert.IsTrue( field.Alias == "alias");
            Assert.IsTrue(field.Type == "type");
            Assert.IsTrue(field.DefaultValues.Count == 0);
            Assert.IsTrue(field.Order == 0);
            Assert.IsTrue(field.ExtraInfo.Length == 0);
        }

        [TestMethod]
        public void SetOrder_Check_Order()
        {
            var field = new Field("name", "alias", "type").SetOrder(1);

            Assert.IsTrue( field.Order == 1);
        }

        [TestMethod]
        public void SetExtraInfo_Check_ExtraInfo()
        {
            var field = new Field("name", "alias", "type").SetExtraInfo("extra");

            Assert.IsTrue( field.ExtraInfo == "extra");
        }

        [TestMethod]
        public void SetDefaultValues_Check_List()
        {
            var defaultValues = new List<string> {"value1", "value2"};
            var field = new Field("name", "alias", "type").SetDefaultValues(new ReadOnlyCollection<string>(defaultValues));

            Assert.IsTrue( field.DefaultValues.Count == 2);
            Assert.IsTrue(field.DefaultValues.Contains("value1"));
            Assert.IsTrue(field.DefaultValues.Contains("value2"));
        }
    }
}