﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Models.Inbound;

namespace Modomodo.Portal.Logging.Tests.Models.Inbound
{
    [TestClass]
    public class SimpleLinkTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewSimpleLink_With_FieldName_Empty_Should_Return_ArgumentNullException()
        {
            new SimpleLink(string.Empty);
        }

        [TestMethod]
        public void VerifyName_With_Wrong_Name_Should_Return_False()
        {
            var simpleLink = new SimpleLink("level") {Name = "fake", Values = new List<string> {"value1"}};
            var result = simpleLink.VerifyName();

            Assert.IsFalse( result);
        }
    }
}
