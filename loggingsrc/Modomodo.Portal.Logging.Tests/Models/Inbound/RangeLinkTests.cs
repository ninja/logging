﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Models.Inbound;

namespace Modomodo.Portal.Logging.Tests.Models.Inbound
{
    [TestClass]
    public class RangeLinkTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void New_RangeLink_With_FieldName_Empty_Should_Return_ArgumentNullException()
        {
            new RangeLink(string.Empty);
        }
    }
}
