﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.Models.Inbound;

namespace Modomodo.Portal.Logging.Tests.Models.Inbound
{
    [TestClass]
    public class ComplexLinkTests
    {
        [TestMethod]
        public void VerifyName_With_Wrong_Name_Should_Return_False()
        {
            var complexLink = new ComplexLink { Name = "fake" };
            var result = complexLink.VerifyName();

            Assert.IsFalse(result);
        }
    }
}
