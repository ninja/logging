﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Modomodo.Portal.Logging.Model.Tests
{
    [TestClass]
    public class LookupsTests
    {
        [TestMethod]
        public void LookupTags_Get_TagTest_Should_Return_Test()
        {
            var result = LookupTags.TagTest;
            Assert.IsTrue( result == "test");
        }

        [TestMethod]
        public void LookupTags_Get_TagProd_Should_Return_Prod()
        {
            var result = LookupTags.TagProd;
            Assert.IsTrue( result == "prod");
        }

        [TestMethod]
        public void LookupElasticIndex_Get_IndexNameDev_Should_Return_mmlogdevelopment()
        {
            var result = LookupElasticIndex.IndexNameDev;
            Assert.IsTrue( result == "mmlogdevelopment");
        }

        [TestMethod]
        public void LookupElasticIndex_Get_IndexNameProd_Should_Return_mmlog()
        {
            var result = LookupElasticIndex.IndexNameProd;
            Assert.IsTrue( result == "mmlog");
        }

        [TestMethod]
        [ExpectedException( typeof( Exception))]
        public void GetIndexNameByApplicationTag_With_Tag_Not_Know_Should_Return_Exception()
        {
            LookupElasticIndex.GetIndexNameByApplicationTag("fake");
        }

        [TestMethod]
        public void GetIndexNameByApplicationTag_With_TagProd_Should_Return_IndexNameProd()
        {
            var result = LookupElasticIndex.GetIndexNameByApplicationTag(LookupTags.TagProd);
            Assert.IsTrue( result == LookupElasticIndex.IndexNameProd);
        }

        [TestMethod]
        public void GetIndexNameByApplicationTag_With_TagTest_Should_Return_IndexNameTest()
        {
            var result = LookupElasticIndex.GetIndexNameByApplicationTag(LookupTags.TagTest);
            Assert.IsTrue(result == LookupElasticIndex.IndexNameDev);
        }
    }
}