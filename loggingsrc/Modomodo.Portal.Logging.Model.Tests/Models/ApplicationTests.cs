﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.InternalUtilityTests;
using Modomodo.Portal.Logging.Model.Models;

namespace Modomodo.Portal.Logging.Model.Tests.Models
{
    //TODO: da abilitare appena si rientegra la class Application
    //[TestClass]
    //public class ApplicationTests
    //{
    //    [TestMethod]
    //    public void CreateApplication_Check_Application()
    //    {
    //        var application = Application.CreateApplication(Constants.ApplicationName, LookupTags.TagTest);

    //        Assert.IsNotNull( application);
    //        Assert.IsTrue( application.Name == Constants.ApplicationName);
    //        Assert.IsTrue( application.Tag == LookupTags.TagTest);
    //        Assert.IsTrue( application.ElasticDocumentTypeName == Constants.ApplicationName + LookupTags.TagTest);
    //        Assert.IsTrue( application.ElasticIndexName == Constants.IndexName);
    //    }

    //    [TestMethod]
    //    [ExpectedException( typeof(ArgumentNullException))]
    //    public void CreateApplication_With_Name_Empty_Return_ArgumentNullException()
    //    {
    //        Application.CreateApplication(string.Empty, LookupTags.TagTest);
    //    }

    //    [TestMethod]
    //    [ExpectedException(typeof(ArgumentNullException))]
    //    public void CreateApplication_With_Tag_Empty_Return_ArgumentNullException()
    //    {
    //        Application.CreateApplication("app", string.Empty);
    //    }

    //    [TestMethod]
    //    [ExpectedException(typeof(ArgumentNullException))]
    //    public void SetFilter_With_Filter_Null_Should_Return_ArgumentNullException()
    //    {
    //        var application = Application.CreateApplication("nameapp", LookupTags.TagTest).SetFilter( null);
    //    }

    //    [TestMethod]
    //    public void SetFilter_Check_List_Filters()
    //    {
    //        const string filterName = "filter name";
    //        const string filterBody = "filter body";

    //        var filter = new Filter( filterName, filterBody);
    //        var application = Application.CreateApplication("nameapp", LookupTags.TagTest).SetFilter( filter);

    //        Assert.IsTrue( application.Filters.Count == 1);
    //        Assert.IsTrue(application.Filters.FirstOrDefault().Name == filterName);
    //        Assert.IsTrue(application.Filters.FirstOrDefault().Body == filterBody);
    //    }
    //}
}