﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modomodo.Portal.Logging.InternalUtilityTests;
using Modomodo.Portal.Logging.Model.Models;

namespace Modomodo.Portal.Logging.Model.Tests.Models
{
    [TestClass]
    public class FilterTests
    {
        private readonly string _body = "testbody";
        private readonly string _name = "testname";

        [TestMethod]
        public void NewFilter_Check_Properties()
        {
            var filter = new Filter(_name, _body, Constants.ApplicationName);

            Assert.IsTrue( filter.Name == _name);
            Assert.IsTrue( filter.Body == _body);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewFilter_With_Name_Empty_Should_Return_ArgumentNullException()
        {
            var filter = new Filter(string.Empty, _body, Constants.ApplicationName);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewFilter_With_Body_Empty_Should_Return_ArgumentNullException()
        {
            var filter = new Filter(_name, string.Empty, Constants.ApplicationName);
        }

        [TestMethod]
        [ExpectedException( typeof(ArgumentNullException))]
        [Ignore]
        public void NewFilter_With_Application_Empty_Should_Return_ArgumentNullException()
        {
            var filter = new Filter("name", "body", string.Empty);
        }
    }
}