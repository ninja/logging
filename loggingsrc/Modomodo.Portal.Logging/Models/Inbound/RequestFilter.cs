﻿using System.ComponentModel.DataAnnotations;

namespace Modomodo.Portal.Logging.Models.Inbound
{
    public class RequestFilter
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public Chain Chain { get; set; }
        [Required]
        public string ApplicationName { get; set; }        
    }
}