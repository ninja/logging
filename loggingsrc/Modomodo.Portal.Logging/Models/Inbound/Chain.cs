﻿using Newtonsoft.Json;

namespace Modomodo.Portal.Logging.Models.Inbound
{
    public class Chain
    {        
        public Link Link { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings {TypeNameHandling = TypeNameHandling.Auto});
        }
    }
}