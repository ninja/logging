﻿using System;
using Modomodo.Portal.Logging.Models.Outbound;

namespace Modomodo.Portal.Logging.Models.Inbound
{
    public class RangeLink : Link
    {
        public RangeLink(string fieldName)
        {
            if( string.IsNullOrWhiteSpace(fieldName))
                throw new ArgumentNullException(nameof(fieldName));
            FieldName = fieldName;
        }

        public string FieldName { get; set; }
        public Range First { get; set; }
        public Range Second { get; set; }
        public override bool VerifyName()
        {
            return Name == ConfigurationRangeCriterion.Name;
        }
    }

    public class Range
    {
        public string Symbol { get; set; }
        public string Value { get; set; }
    }
}