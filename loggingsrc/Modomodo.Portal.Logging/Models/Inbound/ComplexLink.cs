﻿using System.Collections.Generic;
using Modomodo.Portal.Logging.Models.Outbound;

namespace Modomodo.Portal.Logging.Models.Inbound
{
    public class ComplexLink : Link
    {
        public ComplexLink()
        {
            Links = new List<Link>();
        }

        public List<Link> Links { get; set; }
        public override bool VerifyName()
        {
            var allComplexLinkNames = new List<string>
            {
                ConfigurationAndCriterion.Name,
                ConfigurationOrCriterion.Name,
                ConfigurationNotCriterion.Name
            };
            return allComplexLinkNames.Contains(Name);
        }
    }
}