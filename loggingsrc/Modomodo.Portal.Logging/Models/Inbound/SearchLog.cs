﻿namespace Modomodo.Portal.Logging.Models.Inbound
{
    public class SearchLog
    {
        public Chain Chain { get; set; }
        public string IndexName { get; set; }
        public string DocumentTypeName { get; set; }
    }
}