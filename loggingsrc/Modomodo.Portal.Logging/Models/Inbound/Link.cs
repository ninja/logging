﻿
namespace Modomodo.Portal.Logging.Models.Inbound
{
    public abstract class Link
    {
        public string Name { get; set; }

        public abstract bool VerifyName();
    }
}