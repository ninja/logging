﻿using System;
using System.Collections.Generic;
using Modomodo.Portal.Logging.Models.Outbound;

namespace Modomodo.Portal.Logging.Models.Inbound
{
    public class SimpleLink : Link
    {
        public SimpleLink(string fieldName)
        {
            if( string.IsNullOrWhiteSpace(fieldName))
                throw new ArgumentNullException(nameof(fieldName));
            FieldName = fieldName;
            Values = new List<string>();
        }

        public string FieldName { get; set; }
        public List<string> Values { get; set; }

        public override bool VerifyName()
        {
            var allSimpleLinkNames = new List<string>
            {
                ConfigurationTermCriterion.Name,
                ConfigurationMultipleExactTermCriterion.Name,
                ConfigurationMatchCriterion.Name
            };
            return allSimpleLinkNames.Contains(Name);
        }
    }
}