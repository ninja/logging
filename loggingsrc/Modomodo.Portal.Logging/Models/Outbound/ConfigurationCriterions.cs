﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Modomodo.Portal.Logging.Models.Inbound;

namespace Modomodo.Portal.Logging.Models.Outbound
{
    public static class UtilityConfiguationCriterions
    {
        public static ReadOnlyCollection<string> Empty()
        {
            return new ReadOnlyCollection<string>(new List<string>());
        } 
    }

    public static class ConfigurationAndCriterion
    {
        public const string Name = "must";
        public static string Alias => "AND";

        public static Type TypeLink => typeof (ComplexLink);

        public static ReadOnlyCollection<string> Operators => UtilityConfiguationCriterions.Empty();
        public static ReadOnlyCollection<string> PossibleJoints => new ReadOnlyCollection<string>(
            new List<string>
            {
                Name,
                ConfigurationOrCriterion.Name,
                ConfigurationNotCriterion.Name,
                ConfigurationMatchCriterion.Name,
                ConfigurationRangeCriterion.Name,
                ConfigurationMultipleExactTermCriterion.Name,
                ConfigurationTermCriterion.Name
            });
    }

    public static class ConfigurationOrCriterion
    {
        public const string Name = "should";
        public static string Alias => "OR";
        public static Type TypeLink => typeof(ComplexLink);
        public static ReadOnlyCollection<string> Operators => UtilityConfiguationCriterions.Empty();
        public static ReadOnlyCollection<string> PossibleJoints => new ReadOnlyCollection<string>(
            new List<string> { Name,
                ConfigurationAndCriterion.Name,
                ConfigurationNotCriterion.Name,
                ConfigurationMatchCriterion.Name,
                ConfigurationRangeCriterion.Name,
                ConfigurationMultipleExactTermCriterion.Name,
                ConfigurationTermCriterion.Name });
    }

    public static class ConfigurationNotCriterion
    {
        public const string Name = "must_not";
        public static string Alias => "NOT";
        public static Type TypeLink => typeof(ComplexLink);
        public static ReadOnlyCollection<string> Operators => UtilityConfiguationCriterions.Empty();
        public static ReadOnlyCollection<string> PossibleJoints => new ReadOnlyCollection<string>(
            new List<string> { Name,
                ConfigurationAndCriterion.Name,
                ConfigurationOrCriterion.Name,
                ConfigurationMatchCriterion.Name,
                ConfigurationRangeCriterion.Name,
                ConfigurationMultipleExactTermCriterion.Name,
                ConfigurationTermCriterion.Name });
    }

    public static class ConfigurationRangeCriterion
    {
        public const string Name = "range";
        public static string Alias => "RANGE";
        public static Type TypeLink => typeof(RangeLink);
        public static ReadOnlyCollection<string> Operators => new ReadOnlyCollection<string>(new List<string> {">", "<", ">=", "<="});
        public static ReadOnlyCollection<string> PossibleJoints => UtilityConfiguationCriterions.Empty();
    }

    public static class ConfigurationMatchCriterion
    {
        public const string Name = "match";
        public static string Alias => "LIKE";
        public static Type TypeLink => typeof(SimpleLink);
        public static ReadOnlyCollection<string> Operators => UtilityConfiguationCriterions.Empty();
        public static ReadOnlyCollection<string> PossibleJoints => UtilityConfiguationCriterions.Empty();
    }

    public class ConfigurationMultipleExactTermCriterion
    {
        public const string Name = "terms";
        public static string Alias => "IN";
        public static Type TypeLink => typeof(SimpleLink);
        public static ReadOnlyCollection<string> Operators => new ReadOnlyCollection<string>(new List<string> { "="});
        public static ReadOnlyCollection<string> PossibleJoints => UtilityConfiguationCriterions.Empty();
    }

    public class ConfigurationTermCriterion
    {
        public const string Name = "term";
        public static string Alias => "EQUAL";
        public static Type TypeLink => typeof(SimpleLink);
        public static ReadOnlyCollection<string> Operators => new ReadOnlyCollection<string>(new List<string> { "=" });
        public static ReadOnlyCollection<string> PossibleJoints => UtilityConfiguationCriterions.Empty();
    }
}