﻿using System;
using Modomodo.Portal.Logging.Models.Inbound;
using Newtonsoft.Json;

namespace Modomodo.Portal.Logging.Models.Outbound
{
    public class Filter
    {
        private readonly string _name;

        public Filter(string name, string body)
        {
            if( string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name));
            if( string.IsNullOrWhiteSpace( body))
                throw new ArgumentNullException(nameof(name));

            _name = name;
            GetChainByBody( body);
        }

        public string Name => _name;
        public Chain Body { get; private set; }

        private void GetChainByBody(string body)
        {
            Body = JsonConvert.DeserializeObject<Chain>(body, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto });
        }
    }
}