﻿using System;
using System.Collections.Generic;

namespace Modomodo.Portal.Logging.Models.Outbound
{
    public class Field
    {
        private readonly string _name;
        private readonly string _alias;
        private readonly string _type;

        public Field(string name, string alias, string type)
        {
            if( string.IsNullOrWhiteSpace( name))
                throw new ArgumentNullException(nameof(name));
            if( string.IsNullOrWhiteSpace( alias))
                throw new ArgumentNullException(nameof(alias));
            if( string.IsNullOrWhiteSpace( type))
                throw new ArgumentNullException(nameof(type));
            _name = name;
            _alias = alias;
            _type = type;
            DefaultValues = new List<string>();
            ExtraInfo = string.Empty;
        }

        public string Name => _name;
        public string Alias => _alias;
        public string Type => _type;

        public IList<string> DefaultValues { get; private set; } 

        public int Order { get; private set; }

        public string ExtraInfo { get; private set; }

        public Field SetOrder(int order)
        {
            Order = order;
            return this;
        }

        public Field SetExtraInfo(string description)
        {
            ExtraInfo = description;
            return this;
        }

        public Field SetDefaultValues(IReadOnlyCollection<string> defaultValues)
        {
            if (defaultValues != null)
            {
                foreach (var value in defaultValues)
                    DefaultValues.Add(value);
            }
            return this;
        }
    }
}