﻿using System;
using System.Collections.Generic;

namespace Modomodo.Portal.Logging.Models.Outbound
{
    public class Criterion
    {
        private readonly string _name;
        private readonly string _alias;
        private readonly string _assemblyQualifiedName;

        public Criterion(string name, string alias, Type typeLink)
        {
            if( string.IsNullOrWhiteSpace( name))
                throw new ArgumentNullException( nameof(name));

            if( string.IsNullOrWhiteSpace( alias))
                throw new ArgumentNullException( nameof(alias));

            if( typeLink == null)
                throw new ArgumentNullException(nameof(typeLink));

            _name = name;
            _alias = alias;
            _assemblyQualifiedName = typeLink.AssemblyQualifiedName;
            PossibleJoints = new List<string>();
            Operators= new List<string>();
        }

        public string Name => _name;

        public string Alias => _alias;

        public string AssemblyQualifiedName => _assemblyQualifiedName;

        public IList<string> PossibleJoints { get; set; }

        public IList<string> Operators { get; set; }

        public Criterion JointCriterionName(IReadOnlyCollection<string> names)
        {
            if (names != null)
            {
                foreach (var name in names)
                    PossibleJoints.Add(name);
            }
            return this;
        }

        public Criterion SetOperators(IReadOnlyCollection<string> operators)
        {
            if (operators != null)
            {
                foreach (var nameOperator in operators)
                    Operators.Add(nameOperator);
            }
            return this;
        }
    }
}