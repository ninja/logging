﻿using System;

namespace Modomodo.Portal.Logging.Models.Outbound
{
    public class LogMessage
    {
        public string Level { get; set; }
        public string BodyRequest { get; set; }
        public string Keywords { get; set; }
        public string MachineName { get; set; }
        public string CodeResult { get; set; }
        public string UserAgent { get; set; }
        public string HttpRequestUrl { get; set; }
        public DateTimeOffset Date { get; set; }
        public string HttpMethod { get; set; }
        public string Message { get; set; }
        public string ClientHostIp { get; set; }
        public string UserCorrelationId { get; set; }
        public string ExceptionMessage { get; set; }
        public string ExpectionType { get; set; }
        public string StackTrace { get; set; }
        public string ExceptionResult { get; set; }
        public string ExceptionTargetSite { get; set; }
        public string ExceptionSource { get; set; }
        public string ExceptionData { get; set; }
        public string ExceptionHelpLink { get; set; }
        public string InnerException { get; set; }
    }
}