﻿using System.Collections.Generic;
using Modomodo.Portal.Logging.Models.Inbound;

namespace Modomodo.Portal.Logging.Models.Outbound
{
    public static class Fakes
    {
        public static Chain GetFakeChain()
        {
            var chain = new Chain { Name = "test chain" };
            //var complexAnd = new ComplexLink { Name = ConfigurationAndCriterion.Name };
            //var complexOr = new ComplexLink { Name = ConfigurationOrCriterion.Name };
            //complexOr.Links.Add(new SimpleLink { FieldName = "level", Name = ConfigurationTermCriterion.Name, Values = new List<string> { "debug" } });
            //complexOr.Links.Add(new RangeLink
            //{
            //    FieldName = "fields.messagetemplate.Exception.HResult",
            //    Name = ConfigurationRangeCriterion.Name,
            //    First = new Range { Symbol = ">", Value = "10" },
            //    Second = new Range { Symbol = "<=", Value = "8" }
            //});
            //complexAnd.Links.Add(complexOr);
            //complexAnd.Links.Add(new SimpleLink { FieldName = "fields.messagetemplate.Message", Name = ConfigurationMatchCriterion.Name, Values = new List<string> { "error" } });

            chain.Link = new SimpleLink( "fields.messagetemplate.Message") { Name = ConfigurationMatchCriterion.Name, Values = new List<string> { "error" } };
            return chain;
        }
    }
}