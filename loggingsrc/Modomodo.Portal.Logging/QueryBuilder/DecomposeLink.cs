﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Modomodo.Portal.Logging.Models.Inbound;
using Modomodo.Portal.Logging.Models.Outbound;
using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Query;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;
using Modomodo.Portal.Logging.Query.Elastic.Models.Visitor;

namespace Modomodo.Portal.Logging.QueryBuilder
{
    public class DecomposeLink
    {
        private readonly ElasticSearchQueryVisitor _elasticSearchQueryVisitor;
        private readonly ICriteria _chainSearchCriteria;
        private List<AbstractCriterion> _criterions;
        private readonly Chain _chain;

        public DecomposeLink( ICriteria criteria, Chain chain)
        {
            _chain = chain;
            ValidationChain();
            _elasticSearchQueryVisitor = new ElasticSearchQueryVisitor();
            _chainSearchCriteria = criteria;
            _criterions = Decompose();
        }

        private void ValidationChain()
        {
            if (_chain.Link == null) return;
            var linkType = _chain.Link.GetType();

            if (linkType == typeof (SimpleLink))
                ValidateSimpleLink((SimpleLink)_chain.Link);
            if (linkType == typeof (RangeLink))
                ValidateRangeLink((RangeLink)_chain.Link);
            if (linkType == typeof (ComplexLink))
                ValidateComplexLink((ComplexLink)_chain.Link);
        }

        private void ValidateSimpleLink(SimpleLink simpleLink)
        {
            if (simpleLink.VerifyName())
                return;
            throw new System.Exception( nameof(simpleLink.Name) + " not found");
        }

        private void ValidateRangeLink(RangeLink rangeLink)
        {
            if( !rangeLink.VerifyName())
                throw new System.Exception(nameof(rangeLink.Name) + " not found");
            if( rangeLink.First == null)
                throw new ArgumentNullException( nameof(rangeLink.First));
        }

        private void ValidateComplexLink(ComplexLink complexLink)
        {
            if( !complexLink.VerifyName())
                throw new System.Exception(nameof(complexLink.Name) + " not found");
            if (complexLink.Links.Count != 2 && ( complexLink.Name == ConfigurationAndCriterion.Name || complexLink.Name == ConfigurationOrCriterion.Name))
                throw new System.Exception("This complex link combines only two criterions");
            if( complexLink.Links.Count != 1 && complexLink.Name == ConfigurationNotCriterion.Name)
                throw new System.Exception("This complex link combines only one criterion");
        }

        private static AbstractCriterion ManageSimpleLink(SimpleLink simpleLink)
        {
            var firstValue = string.Empty;
            if (simpleLink.Values.FirstOrDefault() != null)
                firstValue = simpleLink.Values.First();
            var name = simpleLink.Name;
            switch (name)
            {
                case ConfigurationTermCriterion.Name:
                        return new TermCriterion(ElasticProperty.Create<WebMessageTemplate>(simpleLink.FieldName), ElasticObject.Create(firstValue));
                case ConfigurationMultipleExactTermCriterion.Name:
                {
                    var elasticObjects = simpleLink.Values.Select(value => ElasticObject.Create(value)).ToList();
                    return new MultipleExactTermCriterion(ElasticProperty.Create<WebMessageTemplate>(simpleLink.FieldName), elasticObjects);
                }
                case ConfigurationMatchCriterion.Name:
                    return new MatchCriterion(ElasticProperty.Create<WebMessageTemplate>(simpleLink.FieldName), ElasticObject.Create(firstValue));
            }
            throw new System.Exception(nameof(simpleLink.Name) + " not found");
        }

        private static AbstractCriterion ManageRangeLink(RangeLink rangeLink)
        {
            RangeSpecification firstSpecification = null;
            RangeSpecification secondSpecification = null;
            if( rangeLink.First != null)
                firstSpecification = new RangeSpecification( RangeComparison.FromSymbolToRangeComparison( rangeLink.First.Symbol), ElasticObject.Create( rangeLink.First.Value));

            if (rangeLink.Second != null)
                secondSpecification = new RangeSpecification(RangeComparison.FromSymbolToRangeComparison(rangeLink.Second.Symbol),
                        ElasticObject.Create(rangeLink.Second.Value));
            
            if( firstSpecification != null && secondSpecification != null)
                return new RangeCriterion( ElasticProperty.Create<WebMessageTemplate>(rangeLink.FieldName), firstSpecification, secondSpecification);
            if( firstSpecification != null)
                return new RangeCriterion( ElasticProperty.Create<WebMessageTemplate>(rangeLink.FieldName), firstSpecification);
            
            throw new System.Exception("error builder range criterion");
        }

        public string GetFilterByLinks()
        {
            foreach (var criterion in _criterions)
                _chainSearchCriteria.AddCriterion(criterion);

            _chainSearchCriteria.Accept(_elasticSearchQueryVisitor);
            return _elasticSearchQueryVisitor.Filter;
        }

        private List<AbstractCriterion> Decompose()
        {
            _criterions = new List<AbstractCriterion>();
            var link = _chain.Link;
            if (link == null)
                return _criterions;

            var stackCriterions = new Stack();
            ManagePushSimpleLink( link, stackCriterions);
            if (link.GetType() == typeof (ComplexLink))
            {
                var traversalChain = DepthFirstTreeTraversal(link, l => ((ComplexLink) l).Links).ToList();
                traversalChain.Reverse();
                foreach (var navigateLink in traversalChain)
                {
                    var navigateLinkType = navigateLink.GetType();
                    ManagePushSimpleLink(navigateLink, stackCriterions);
                    if (navigateLinkType == typeof (ComplexLink))
                        ManagePushComplexLink((ComplexLink) navigateLink, stackCriterions);
                }
            }
            foreach (var stackCriterion in stackCriterions)
                _criterions.Add((AbstractCriterion) stackCriterion);
            return _criterions;
        }

        public void ManagePushSimpleLink(Link link, Stack stackCriterions)
        {
            var linkType = link.GetType();
            if (linkType == typeof(SimpleLink))
                stackCriterions.Push(ManageSimpleLink((SimpleLink)link));
            if (linkType == typeof(RangeLink))
                stackCriterions.Push(ManageRangeLink((RangeLink)link));
        }

        private void ManagePushComplexLink(ComplexLink navigateLink, Stack stackCriterions)
        {
            if (navigateLink.Name == ConfigurationAndCriterion.Name)
            {
                var andCriterion = new AndCriterion((AbstractCriterion)stackCriterions.Pop(), (AbstractCriterion)stackCriterions.Pop());
                stackCriterions.Push(andCriterion);
            }
            if (navigateLink.Name == ConfigurationOrCriterion.Name)
            {
                var orCriterion = new OrCriterion((AbstractCriterion)stackCriterions.Pop(), (AbstractCriterion)stackCriterions.Pop());
                stackCriterions.Push(orCriterion);
            }
            if (navigateLink.Name == ConfigurationNotCriterion.Name)
            {
                var notCriterion = new NotCriterion((AbstractCriterion)stackCriterions.Pop());
                stackCriterions.Push(notCriterion);
            }
        }

        private static IEnumerable<Link> DepthFirstTreeTraversal(Link root, Func<Link, IEnumerable<Link>> children)
        {
            var stack = new Stack<Link>();
            stack.Push(root);
            while (stack.Count != 0)
            {
                var current = stack.Pop();
                if (current.GetType() == typeof(ComplexLink))
                {
                    foreach (var child in children(current).Reverse())
                        stack.Push(child);
                }
                yield return current;
            }
        }
    }
}