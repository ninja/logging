﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Modomodo.Portal.Logging.Data.Interfaces;
using Modomodo.Portal.Logging.Filters;
using Modomodo.Portal.Logging.Model.Models;
using Modomodo.Portal.Logging.Models.Inbound;

namespace Modomodo.Portal.Logging.Controllers
{
    public class FilterController : BaseController
    {
        private readonly IFilterRepository _filterRepository;

        public FilterController(IFilterRepository filterRepository)
        {
            _filterRepository = filterRepository;
        }

        [HttpGet]
        [V1Route("filters")]
        //[IdentityAuthenticate] //TODO: da riattivare
        public JsonResult<List<Models.Outbound.Filter>> GetFilters(string applicationname)
        {
            var filters =_filterRepository.WhereByApplicationName(applicationname);
            //TODO: da spostare in classe di mapper
            if (filters.Any())
            {
                var filterOut = filters.Select(filter => new Models.Outbound.Filter(filter.Name, filter.Body)).ToList();
                return Json(filterOut);
            }
            //TODO da capire cosa restituire
            return null;
        }

        [HttpPost]
        [V1Route("filter")]
        //[IdentityAuthenticate] //TODO: da riattivare
        public IHttpActionResult CreateFilter(RequestFilter request)
        {
            ValidateModel();
            var filter = new Filter(request.Name, request.Chain.ToString(), request.ApplicationName);
            var result = _filterRepository.Insert(filter);
            if( result)
                return Ok();
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = "Try again" });
        }

        [HttpPut]
        [V1Route("filter")]
        //[IdentityAuthenticate] //TODO: da riattivare
        public IHttpActionResult UpdateFilter(RequestFilter request)
        {
            ValidateModel();
            var filter = new Filter( request.Name, request.Chain.ToString(), request.ApplicationName);
            var result = _filterRepository.UpdateBody(filter);
            if( result)
                return Ok();
            throw new HttpResponseException( new HttpResponseMessage( HttpStatusCode.InternalServerError) {ReasonPhrase = "Try again"} );
        }

        [HttpDelete]
        [V1Route("filter")]
        //[IdentityAuthenticate] //TODO: da riattivare
        public IHttpActionResult DeleteFilter(RequestFilter request)
        {
            ValidateModel();
            var filter = new Filter( request.Name, request.Chain.ToString(), request.ApplicationName);
            var result = _filterRepository.Delete(filter);
            if( result)
                return Ok();
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = "Try again" });
        }
    }
}