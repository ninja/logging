﻿using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Modomodo.Security.Service.Web.WebApi.Controller;

namespace Modomodo.Portal.Logging.Controllers
{
    public class BaseController : IdentityController
    {
        protected void ValidateModel()
        {
            if (ModelState.IsValid == false)
                RaiseNotValidHttpException();
        }

        protected void RaiseNotValidHttpException()
        {
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = "Data are not valid" });
        }

        public int MaxDocuments {
            get
            {
                int maxDocuments = 50;
                var value = ConfigurationManager.AppSettings["MaxDocuments"];
                if(value != null)
                    int.TryParse(value, out maxDocuments);
                return maxDocuments;
            }
        }        
    }
}