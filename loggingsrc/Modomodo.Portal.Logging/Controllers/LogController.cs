﻿using Modomodo.Portal.Logging.Filters;
using Modomodo.Portal.Logging.Models.Outbound;
using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Data;
using Modomodo.Portal.Logging.Query.Elastic.Models.Request;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Modomodo.Portal.Logging.Models.Inbound;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query;
using Modomodo.Portal.Logging.QueryBuilder;
using Newtonsoft.Json;
using Exception = System.Exception;

namespace Modomodo.Portal.Logging.Controllers
{
    //TODO: da rivedere
    public class LogController : BaseController
    {
        private readonly ILogRepository _baseLogRepository;
        
        public LogController(ILogRepository baseLogRepository)
        {
            _baseLogRepository = baseLogRepository;
        }

        [HttpGet]
        [V1Route("lastlogs")]
        //[IdentityAuthenticate] //TODO: da riattivare
        public JsonResult<IEnumerable<LogMessage>> GetLastLogs(string indexname, string documenttypename)
        {
            //TODO: da rivedere (mapping campi, document type etc)
            var searchLogRequest = SearchLogRequest.NewSearch(indexname, documenttypename).SetDefaultSortOption();
            searchLogRequest.MaxSizeReturnDocuments = MaxDocuments;
            var webMessageTemplate = _baseLogRepository.GetLastLogs(searchLogRequest);
            return Json(AutoMapper.Mapper.Map<IEnumerable<LogMessage>>(webMessageTemplate));
        }

        [HttpGet]
        [V1Route("fields")]
        //[IdentityAuthenticate] //TODO: da riattivare
        public JsonResult<List<Field>> GetFieldsByWebMessageTemplate()
        {
            var fields = new List<Field>
            {
                new Field("level", "Level", "string").SetOrder(1).SetDefaultValues(new ReadOnlyCollection<string>(new List<string> { "Debug", "Information", "Warning", "Error", "Fatal"})),
                new Field("fields.messagetemplate.HttpRequest", "Http request", "string").SetOrder(2),
                new Field("fields.messagetemplate.Keywords", "Keywords", "string").SetOrder(3),
                new Field("fields.messagetemplate.MachineName", "Machine name", "string").SetOrder(4),
                new Field("fields.messagetemplate.CodeResult", "Code result", "string").SetOrder(5),
                new Field("fields.messagetemplate.UserAgent", "User agent", "string").SetOrder(6),
                new Field("fields.messagetemplate.HttpRequestUrl", "Http url", "string").SetOrder(7),
                new Field("fields.messagetemplate.Date", "Timestamp document", "date").SetOrder(8),
                new Field("fields.messagetemplate.HttpMethod", "Http method", "string").SetOrder(9).SetDefaultValues(new ReadOnlyCollection<string>(new List<string> { "GET", "POST", "PUT", "PATCH", "DELETE", "COPY", "HEAD", "OPTIONS"})),
                new Field("fields.messagetemplate.Message", "Log message", "string").SetOrder(10),
                new Field("fields.messagetemplate.ClientHostIp", "Client ip", "string").SetOrder(11),
                new Field("fields.messagetemplate.UserCorrelationId", "User correlation id", "string").SetOrder(12),
                new Field("fields.messagetemplate.Exception._typeTag", "Exception type", "string").SetOrder(13),
                new Field("fields.messagetemplate.Exception.Message", "Exception message", "string").SetOrder(14),
                new Field("fields.messagetemplate.Exception.StackTrace", "StackTrace", "string").SetOrder(15),
                new Field("fields.messagetemplate.Exception.HResult", "Hresult", "int").SetOrder(16),
                new Field("fields.messagetemplate.Exception.TargetSite", "TargetSite", "string").SetOrder(17),
                new Field("fields.messagetemplate.Exception.Source", "Exception source", "string").SetOrder(18),
                new Field("fields.messagetemplate.Exception.Data", "Exception data", "string").SetOrder(19),
                new Field("fields.messagetemplate.Exception.HelpLink", "Exception help link", "string").SetOrder(20),
                new Field("fields.messagetemplate.Exception.InnerException", "InnerException", "string").SetOrder(21)
            };
            return Json(fields);
        }

        [HttpGet]
        [V1Route("criterions")]
        //[IdentityAuthenticate] //TODO: da riattivare
        public JsonResult<List<Criterion>> GetCriterions()
        {
            var criterions = new List<Criterion>
            {
                new Criterion(ConfigurationAndCriterion.Name, ConfigurationAndCriterion.Alias, ConfigurationAndCriterion.TypeLink)
                    .JointCriterionName(ConfigurationAndCriterion.PossibleJoints)
                    .SetOperators(ConfigurationAndCriterion.Operators),
                new Criterion( ConfigurationOrCriterion.Name, ConfigurationOrCriterion.Alias, ConfigurationOrCriterion.TypeLink)
                    .JointCriterionName(ConfigurationOrCriterion.PossibleJoints)
                    .SetOperators(ConfigurationOrCriterion.Operators),
                new Criterion( ConfigurationNotCriterion.Name, ConfigurationNotCriterion.Alias, ConfigurationNotCriterion.TypeLink)
                    .JointCriterionName(ConfigurationNotCriterion.PossibleJoints)
                    .SetOperators(ConfigurationNotCriterion.Operators),
                new Criterion( ConfigurationMatchCriterion.Name, ConfigurationMatchCriterion.Alias, ConfigurationMatchCriterion.TypeLink)
                    .JointCriterionName(ConfigurationMatchCriterion.PossibleJoints)
                    .SetOperators(ConfigurationMatchCriterion.Operators),
                new Criterion( ConfigurationRangeCriterion.Name, ConfigurationRangeCriterion.Alias, ConfigurationRangeCriterion.TypeLink)
                    .JointCriterionName(ConfigurationRangeCriterion.PossibleJoints)
                    .SetOperators(ConfigurationRangeCriterion.Operators),
                new Criterion( ConfigurationMultipleExactTermCriterion.Name, ConfigurationMultipleExactTermCriterion.Alias, ConfigurationMultipleExactTermCriterion.TypeLink)
                    .JointCriterionName(ConfigurationMultipleExactTermCriterion.PossibleJoints)
                    .SetOperators(ConfigurationMultipleExactTermCriterion.Operators),
                new Criterion( ConfigurationTermCriterion.Name, ConfigurationTermCriterion.Alias, ConfigurationTermCriterion.TypeLink)
                    .JointCriterionName(ConfigurationTermCriterion.PossibleJoints)
                    .SetOperators(ConfigurationTermCriterion.Operators)
            };
            return Json(criterions);
        }

        [HttpGet]
        [V1Route("fakechain")]
        public JsonResult<Chain> FakeChain()
        {
            return Json(Fakes.GetFakeChain(), new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto });
        }

        [HttpPost]
        [V1Route("search")]
        public JsonResult<IEnumerable<WebMessageTemplate>> Search(SearchLog searchLog)
        {
            try
            {
                var decomposeLink = new DecomposeLink(new ChainSearchCriteria(), searchLog.Chain);
                var filterElasticSearch = decomposeLink.GetFilterByLinks();
                var searchLogRequest =
                    SearchLogRequest.NewSearch(searchLog.IndexName, searchLog.DocumentTypeName)
                        .SetDefaultSortOption()
                        .SetFilterRaw(filterElasticSearch);
                searchLogRequest.MaxSizeReturnDocuments = MaxDocuments;
                var logs = _baseLogRepository.SearchLogs(searchLogRequest);
                return Json(logs);
            }
            catch (Exception excpetion)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = "Reset your inputs and try again" });
            }
        } 
    }
}