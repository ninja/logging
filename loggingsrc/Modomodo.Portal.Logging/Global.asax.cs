﻿using Autofac.Integration.WebApi;
using Modomodo.Security.Service.Configuration;
using System.Web.Http;
using Newtonsoft.Json;

namespace Modomodo.Portal.Logging
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var serializerSettings = GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings;
            serializerSettings.TypeNameHandling = TypeNameHandling.Auto;

            //IOC configuration
            var iocConfiguration = new IocConfiguration();
            var container = iocConfiguration.Container;
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            //TODO: gestire chiave in web.config
            IdentityService.Setup(
                IdentityServiceOptions.IsIdentifiedBy("c1b91c3a-4b49-4a18-ad61-054fa1d9ccc8", "log")
                .HasSigningKey(() =>
                {
                    var key = "uVgFb4tpVIsD8-Wfxy4djLrCiXP5DtW0D3PJa39VXvI";
                    return key;
                })
                .HasIssuer("http://security.modomodo.com/ip"));

            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
