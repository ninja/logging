﻿using System;
using System.Web.Http.Routing;

namespace Modomodo.Portal.Logging.Filters
{
    public class V1RouteAttribute : Attribute, IDirectRouteFactory, IHttpRouteInfoProvider
    {
        public V1RouteAttribute()
        {
        }

        public V1RouteAttribute(string template)
        {
            Template = string.Format("api/v1/{0}", template);
        }

        public RouteEntry CreateRoute(DirectRouteFactoryContext context)
        {
            IDirectRouteBuilder builder = context.CreateBuilder(Template);

            builder.Name = Name;
            builder.Order = Order;
            return builder.Build();
        }

        public string Name { get; private set; }
        public string Template { get; private set; }
        public int Order { get; private set; }
    }
}