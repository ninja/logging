﻿using AutoMapper;
using Modomodo.Portal.Logging.Models.Outbound;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using System.Collections.Generic;

namespace Modomodo.Portal.Logging
{
    public class MapperConfiguration
    {
        public static void Configuration()

        {
            Configure();
        }

        private static void Configure()
        {
            Mapper.CreateMap<WebMessageTemplate, LogMessage>()
                .ForMember(dest => dest.Level, expression => expression.MapFrom(x => x.level))
                .ForMember(dest => dest.BodyRequest, expression => expression.MapFrom(x => x.fields.messagetemplate.HttpRequest))
                .ForMember(dest => dest.Keywords, expression => expression.MapFrom(x => ManageKeywordsField(x.fields.messagetemplate.Keywords)))
                .ForMember(dest => dest.MachineName, expression => expression.MapFrom(x => x.fields.messagetemplate.MachineName))
                .ForMember(dest => dest.CodeResult, expression => expression.MapFrom(x => x.fields.messagetemplate.CodeResult))
                .ForMember(dest => dest.UserAgent, expression => expression.MapFrom(x => x.fields.messagetemplate.UserAgent))
                .ForMember(dest => dest.HttpRequestUrl, expression => expression.MapFrom(x => x.fields.messagetemplate.HttpRequestUrl))
                .ForMember(dest => dest.Date, expression => expression.MapFrom(x => x.fields.messagetemplate.Date))
                .ForMember(dest => dest.HttpMethod, expression => expression.MapFrom(x => x.fields.messagetemplate.HttpMethod))
                .ForMember(dest => dest.Message, expression => expression.MapFrom(x => x.fields.messagetemplate.Message))
                .ForMember(dest => dest.ClientHostIp, expression => expression.MapFrom(x => x.fields.messagetemplate.ClientHostIp))
                .ForMember(dest => dest.UserCorrelationId, expression => expression.MapFrom(x => x.fields.messagetemplate.UserCorrelationId))
                .ForMember(dest => dest.ExceptionMessage, expression => expression.MapFrom(x => x.fields.messagetemplate.Exception.Message))
                .ForMember(dest => dest.ExpectionType, expression => expression.MapFrom(x => x.fields.messagetemplate.Exception._typeTag))
                .ForMember(dest => dest.StackTrace, expression => expression.MapFrom(x => x.fields.messagetemplate.Exception.StackTrace))
                .ForMember(dest => dest.ExceptionResult, expression => expression.MapFrom(x => x.fields.messagetemplate.Exception.Hresult))
                .ForMember(dest => dest.ExceptionTargetSite, expression => expression.MapFrom(x => x.fields.messagetemplate.Exception.TargetSite))
                .ForMember(dest => dest.ExceptionSource, expression => expression.MapFrom(x => x.fields.messagetemplate.Exception.Source))
                .ForMember(dest => dest.ExceptionData, expression => expression.MapFrom(x => x.fields.messagetemplate.Exception.Data))
                .ForMember(dest => dest.ExceptionHelpLink, expression => expression.MapFrom(x => x.fields.messagetemplate.Exception.HelpLink))
                .ForMember(dest => dest.InnerException, expression => expression.MapFrom(x => x.fields.messagetemplate.Exception.InnerException));
        }

        private static string ManageKeywordsField( IEnumerable<string> keywords)
        {
            if( keywords == null)
                return string.Empty;
            return string.Join(",", keywords);
        }
    }
}