﻿using System.Web.Http;

namespace Modomodo.Portal.Logging
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();
            MapperConfiguration.Configuration();
        }
    }
}
