﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Autofac;
using Autofac.Integration.WebApi;
using Modomodo.Portal.Logging.Data;
using Modomodo.Portal.Logging.Data.Interfaces;
using Modomodo.Portal.Logging.Query.Elastic.Data;

namespace Modomodo.Portal.Logging
{
    public class IocConfiguration
    {
        private readonly IContainer _container;

        public IocConfiguration()
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterApiControllers(typeof(WebApiApplication).Assembly);
            containerBuilder.RegisterType<LogAppRepository>().AsImplementedInterfaces().InstancePerRequest();
            var connectionString = ConfigurationManager.ConnectionStrings["PortalLoggingConnection"];
            containerBuilder.RegisterInstance(new DapperConnection(new SqlConnection(connectionString.ConnectionString)).Connection).As<IDbConnection>();
            containerBuilder.RegisterType<FilterRepository>().UsingConstructor(typeof(IDbConnection)).As<IFilterRepository>().InstancePerRequest();
            _container = containerBuilder.Build();
        }

        public IContainer Container => _container;
    }
}