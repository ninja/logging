﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using Modomodo.Platform.Stack.Interfaces;
using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Data;
using System.Collections.ObjectModel;

namespace Modomodo.Portal.Logging.Query.Elastic.Data
{
    public sealed class ConnectionString : IElasticConnection
    {
        private ReadOnlyCollection<Uri> _server;

        public ConnectionString(IConfigurationManager configurationManager)
        {
            var connectionString = configurationManager.ConnectionStrings("elasticsearch");
            if( !string.IsNullOrWhiteSpace( connectionString))
                Parse(connectionString);
            else
                throw new ConfigurationErrorsException("The elasticsearch element is not currently associated");
        }

        private void Parse(string originalConnection)
        {
            const string pattern = @"^elastic://" +
                                   @"(?<server>[^/?]+)?";

            var match = Regex.Match(originalConnection, pattern);
            if (!match.Success)
                throw new ConfigurationErrorsException("The connection is not valid.");

            ExtractServer(match.Groups);
        }

        private void ExtractServer(GroupCollection groups)
        {
            var match = groups["server"];
            if (match.Success)
            {
                var nodes = match.Value.Split(',');
                var server = new List<Uri>();

                foreach (var n in nodes)
                {
                    var node = $"http://{n}";
                    var isValidUri = Uri.IsWellFormedUriString(node, UriKind.Absolute);
                    if (isValidUri)
                        server.Add(new Uri(node));
                    else
                        throw new UriFormatException("uri not valid");
                }
                _server = new ReadOnlyCollection<Uri>(server);
            }
            else
                throw new ConfigurationErrorsException("the servers can not be null");
        }
    
        public ReadOnlyCollection<Uri> Server => _server;
    }
}