﻿using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Data;
using Modomodo.Portal.Logging.Query.Elastic.Models;
using System.Collections.Generic;

namespace Modomodo.Portal.Logging.Query.Elastic.Data
{
    public class LogAppRepository : BaseRepository<WebMessageTemplate>, ILogRepository
    {
        public IEnumerable<WebMessageTemplate> GetLastLogs(Models.Request.SearchRequest<WebMessageTemplate> searchRequest)
        {
            var searchDescriptor = GetBaseSearchDescriptor(searchRequest);
            var lastDocumentQuery = Client.Search<WebMessageTemplate>(searchDescriptor);
            return ManageElasticResult(lastDocumentQuery);
        }

        public IEnumerable<WebMessageTemplate> SearchLogs(Models.Request.SearchRequest<WebMessageTemplate> searchRequest)
        {
            var searchDescriptor = GetBaseSearchDescriptor(searchRequest);
            searchDescriptor.FilterRaw(searchRequest.FilterRaw);
            var searchDocuments = Client.Search<WebMessageTemplate>(searchDescriptor);
            return ManageElasticResult(searchDocuments);
        }
    }
}
