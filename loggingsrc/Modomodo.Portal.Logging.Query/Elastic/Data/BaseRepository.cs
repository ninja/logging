﻿using Nest;
using System;
using Elasticsearch.Net.ConnectionPool;
using Modomodo.Platform.Stack.Interfaces;
using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Data;
using System.Linq;
using System.Collections.Generic;

namespace Modomodo.Portal.Logging.Query.Elastic.Data
{
    public abstract class BaseRepository<T> : IBaseRepository
    {
        private readonly IElasticClient _client;

        protected BaseRepository(Uri elastiSearchServerUrl = null)
        {
            if (elastiSearchServerUrl == null)
            {
                var configurationManager = new ConfigurationManagerWrapper();
                IElasticConnection connection = new ConnectionString(configurationManager);
                var connectionPool = new SniffingConnectionPool(connection.Server);
                var settings = new ConnectionSettings(connectionPool);
                _client = new ElasticClient(settings);
            }
            //TODO: da completare
            else
                throw new Exception("todo");
        }

        public IElasticClient Client => _client;

        public long CountDocuments(Models.Request.SearchRequest<T> searchRequest)
        {
            var countQuery = Client.Count<dynamic>(c => c.Index(searchRequest.IndexName).AllTypes().Query(q => q.MatchAll()));
            return countQuery.Count;
        }

        public SearchDescriptor<T> GetBaseSearchDescriptor<T>(Models.Request.SearchRequest<T> searchRequest) where T : class
        {
            var searchDescriptor = new SearchDescriptor<T>();
            searchDescriptor.Index(searchRequest.IndexName);
            searchDescriptor.Type(searchRequest.DocumentTypeName);
            searchDescriptor.Size(searchRequest.MaxSizeReturnDocuments);

            foreach (var fieldName in searchRequest.SortOptions.Where(s => s.Ascending).Select(f => f.FieldName))
                searchDescriptor.SortAscending(fieldName);

            foreach (var fieldName in searchRequest.SortOptions.Where(s => !s.Ascending).Select(f => f.FieldName))
                searchDescriptor.SortDescending(fieldName);

            return searchDescriptor;
        }

        public IEnumerable<T> ManageElasticResult<T>(ISearchResponse<T> searchDocuments) where T : class
        {
            //TODO: da completare
            var connectionStatus = ((BaseResponse)searchDocuments).ConnectionStatus;
            if (connectionStatus.HttpStatusCode.HasValue && connectionStatus.HttpStatusCode.Value == 200)
                return searchDocuments.Documents;

            throw new NotImplementedException();
        }
    }
}