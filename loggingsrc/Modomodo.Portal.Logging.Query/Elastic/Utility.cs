﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Modomodo.Portal.Logging.Query.Elastic
{
    public static class Utility
    {
        public static string ExtractExpression<T, TProperty>( this Expression<Func<T, TProperty>> action)
        {
            var memberExpression = action.Body as MemberExpression;
            if (memberExpression == null)
                return null;

            var body = memberExpression;
            var expression = body.ToString();

            var parts = expression.Split('.').Skip(1).ToArray();
            expression = string.Join(".", parts);
            return expression;
        }
    }
}
