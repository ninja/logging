﻿using Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion;

namespace Modomodo.Portal.Logging.Query.Elastic.Interfaces.Visitor
{
    public interface IVisitor
    {
        void Build(AbstractCriterion searchCriterion);
    }
}