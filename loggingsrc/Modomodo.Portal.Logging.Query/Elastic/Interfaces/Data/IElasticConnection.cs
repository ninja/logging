﻿using System;
using System.Collections.ObjectModel;

namespace Modomodo.Portal.Logging.Query.Elastic.Interfaces.Data
{
    public interface IElasticConnection
    {
        ReadOnlyCollection<Uri> Server { get; }
    }
}
