﻿using Modomodo.Portal.Logging.Query.Elastic.Models;
using Modomodo.Portal.Logging.Query.Elastic.Models.Request;
using System.Collections.Generic;

namespace Modomodo.Portal.Logging.Query.Elastic.Interfaces.Data
{
    public interface ILogRepository : IBaseRepository
    {
        long CountDocuments(SearchRequest<WebMessageTemplate> searchRequest);
        IEnumerable<WebMessageTemplate> GetLastLogs(SearchRequest<WebMessageTemplate> searchRequest);
        IEnumerable<WebMessageTemplate> SearchLogs(SearchRequest<WebMessageTemplate> searchRequest);
    }
}
