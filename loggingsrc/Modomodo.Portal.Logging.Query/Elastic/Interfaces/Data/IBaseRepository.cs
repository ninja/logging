﻿namespace Modomodo.Portal.Logging.Query.Elastic.Interfaces.Data
{
    public interface IBaseRepository
    {
        Nest.IElasticClient Client { get; }
    }
}
