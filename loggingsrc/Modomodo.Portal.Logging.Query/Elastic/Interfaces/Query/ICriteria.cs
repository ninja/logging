﻿using System.Collections.ObjectModel;
using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Visitor;

namespace Modomodo.Portal.Logging.Query.Elastic.Interfaces.Query
{
    public interface ICriteria
    {
        ICriteria AddCriterion(ISearchCriterion searchCriterion);
        ReadOnlyCollection<ISearchCriterion> ListCriterion();
        void Accept(IVisitor visitor);
    }
}