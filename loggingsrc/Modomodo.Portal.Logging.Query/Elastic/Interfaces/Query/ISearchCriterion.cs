﻿using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Visitor;

namespace Modomodo.Portal.Logging.Query.Elastic.Interfaces.Query
{
    public interface ISearchCriterion
    {
        void Accept(IVisitor visitor);
    }
}