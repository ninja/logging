﻿using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Elastic.Interfaces.Query
{
    public interface ITermCriterion : ISearchCriterion
    {
        ElasticProperty Field { get; }
        ElasticObject Value { get; }
    }
}