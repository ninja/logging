﻿using System;
using System.Linq.Expressions;

namespace Modomodo.Portal.Logging.Query.Elastic.Interfaces.Configuration
{
    public interface IElasticMapping<T>
    {
        IElasticMapping<T> SetMapping<TProperty>(Expression<Func<T, TProperty>> action, IBaseElasticTypeAnalyzer typeAnalyzer);
    }
}