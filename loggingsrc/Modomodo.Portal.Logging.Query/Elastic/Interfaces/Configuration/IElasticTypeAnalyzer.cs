﻿namespace Modomodo.Portal.Logging.Query.Elastic.Interfaces.Configuration
{
    public interface IElasticTypeAnalyzer : IBaseElasticTypeAnalyzer
    {
        bool LowerCases { get; }
    }
}