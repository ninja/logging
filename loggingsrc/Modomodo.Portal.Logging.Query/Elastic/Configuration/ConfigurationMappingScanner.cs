﻿using Modomodo.Portal.Logging.Query.Elastic.Models.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Modomodo.Portal.Logging.Query.Elastic.Configuration
{
    internal class ConfigurationMappingScanner : IEnumerable<ConfigurationMappingResult>
    {
        private readonly IEnumerable<Type> _types;
        public ConfigurationMappingScanner(IEnumerable<Type> types)
        {
            _types = types;
        }

        public static ConfigurationMappingScanner FindMappingInAssembly(Assembly assembly)
        {
            return new ConfigurationMappingScanner(assembly.GetExportedTypes());
        }

        public IEnumerator<ConfigurationMappingResult> GetEnumerator()
        {
            return Execute().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private IEnumerable<ConfigurationMappingResult> Execute()
        {
            var genericType = typeof(AbstractConfigurationElasticMapping<>);

            var query = from type in _types
                        where type.IsClass &&
                        type.BaseType != null &&
                        type.BaseType.IsGenericType &&
                        type.BaseType.GetGenericTypeDefinition() == genericType
                        select new ConfigurationMappingResult(type);
            return query;
        }
    }
}
