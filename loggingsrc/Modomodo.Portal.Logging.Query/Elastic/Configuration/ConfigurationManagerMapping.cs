﻿using Modomodo.Portal.Logging.Query.Elastic.Models.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

namespace Modomodo.Portal.Logging.Query.Elastic.Configuration
{
    public class ConfigurationManagerMapping
    {
        private static ConfigurationManagerMapping _instance;
        private readonly IList<ConfigurationMappingResult> _listMapping;

        protected ConfigurationManagerMapping()
        {
            var scanner = ConfigurationMappingScanner.FindMappingInAssembly(Assembly.GetExecutingAssembly());
            _listMapping = scanner.ToList();
        }

        public static ConfigurationManagerMapping GetInstance()
        {
            return _instance ?? (_instance = new ConfigurationManagerMapping());
        }

        public ReadOnlyCollection<ConfigurationMappingResult> ListMapping()
        {
            return new ReadOnlyCollection<ConfigurationMappingResult>(_listMapping);
        }

        public Type GetConfigurationMapping<T>()
        {
            var configurationMapping = from c in _listMapping
                                       where c.MappingType.BaseType != null &&
                                       c.MappingType.BaseType.GenericTypeArguments.FirstOrDefault(g => g == typeof(T)) != null
                                       select c.MappingType;
            return configurationMapping.FirstOrDefault();
        }
    }
}
