﻿using Modomodo.Portal.Logging.Query.Elastic.Configuration;
using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Configuration;
using Modomodo.Portal.Logging.Query.Elastic.Models.Configuration;
using System;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Search
{
    public class ElasticProperty
    {
        private readonly string _fieldName;
        private readonly IBaseElasticTypeAnalyzer _analyzer;

        private ElasticProperty(string fieldName, IBaseElasticTypeAnalyzer analyzer)
        {
            if (string.IsNullOrWhiteSpace(fieldName))
                throw new ArgumentNullException(fieldName);

            _fieldName = fieldName;
            _analyzer = analyzer;
        }

        public string FieldName => _fieldName;

        public IBaseElasticTypeAnalyzer Analyzer => _analyzer;

        public static ElasticProperty Create<T>(string fieldName)
        {
            var analyzer = ElasticTypeAnalyzer.None;
            var configurationMapping = ConfigurationManagerMapping.GetInstance().GetConfigurationMapping<T>();
            if( configurationMapping != null)
            {
                var instanceConfigurationMapping = Activator.CreateInstance(configurationMapping);
                analyzer = ((AbstractElasticTypeAnalyzer)instanceConfigurationMapping).GetTypeAnalyzerByField(fieldName);
            }
            return new ElasticProperty(fieldName, analyzer);
        }
    }
}