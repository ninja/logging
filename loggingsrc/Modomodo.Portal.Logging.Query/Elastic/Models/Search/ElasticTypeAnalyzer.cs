﻿using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Configuration;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Search
{
    public class ElasticTypeAnalyzer : IElasticTypeAnalyzer
    {
        private readonly bool _lowerCases;

        private ElasticTypeAnalyzer( bool lowerCases)
        {
            _lowerCases = lowerCases;
        }

        public bool LowerCases => _lowerCases;

        public static IBaseElasticTypeAnalyzer None => new ElasticNullAnalyzer();

        public static IBaseElasticTypeAnalyzer Standard => new ElasticTypeAnalyzer(true);
        
        public static IBaseElasticTypeAnalyzer Simple => new ElasticTypeAnalyzer(true);

        public static IBaseElasticTypeAnalyzer Whitespace => new ElasticTypeAnalyzer(false);

        public static IBaseElasticTypeAnalyzer English => new ElasticTypeAnalyzer(true);
    }
}