﻿using System.Collections.Generic;
using System.Linq;
using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Configuration;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Search
{
    public class ElasticObject
    {
        private readonly string _originalValue;
        private static readonly List<string> ReservedCharacters = new List<string> { "+", "-", "=", "&&", "||", ">", "<", "!", "(", ")", "{", "}", "[", "]", "^", "\"", "~", "*", "?", ":", "\\", "/" };

        private ElasticObject( string originalValue)
        {
            _originalValue = originalValue;
        }

        public string OriginalValue => _originalValue;

        public string TrasformationValue(IBaseElasticTypeAnalyzer typeAnalyzer)
        {
            if ( typeAnalyzer != null && typeAnalyzer.GetType() != typeof(ElasticNullAnalyzer) && ((IElasticTypeAnalyzer)typeAnalyzer).LowerCases)
                return _originalValue.ToLower();
            return _originalValue;
        }

        public static ElasticObject Create( string originalValue)
        {
            //if (!IsCorrectValue(originalValue))
            //    throw new System.Exception(nameof(originalValue) + " cannot parse");
            return new ElasticObject(originalValue);
        }

        private static bool IsCorrectValue(string value)
        {
            return ReservedCharacters.All(invalidCharacter => value.IndexOf(invalidCharacter) == -1);
        }
    }
}