﻿using System.Collections.ObjectModel;
using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Query;
using System.Collections.Generic;
using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Visitor;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Query
{
    public class ChainSearchCriteria : ICriteria
    {
        private readonly List<ISearchCriterion> _criteria;

        public ChainSearchCriteria()
        {
            _criteria = new List<ISearchCriterion>();
        }

        public ICriteria AddCriterion(ISearchCriterion searchCriterion)
        {
            _criteria.Add(searchCriterion);
            return this;
        }

        public ReadOnlyCollection<ISearchCriterion> ListCriterion()
        {
            return new ReadOnlyCollection<ISearchCriterion>(_criteria);
        }

        public void Accept(IVisitor visitor)
        {
            var list = ListCriterion();
            foreach (var criterion in list)
                criterion.Accept(visitor);
        }     
    }
}
