﻿using System;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Query
{
    public sealed class RangeComparison
    {
        public string Value { get; private set; }

        public RangeComparison(string value)
        {
            Value = value;
        }

        public static RangeComparison GreaterThan => new RangeComparison("gt");

        public static RangeComparison GreaterThanOrEqual => new RangeComparison("gte");

        public static RangeComparison LessThan => new RangeComparison("lt");

        public static RangeComparison LessThanOrEqual => new RangeComparison("lte");

        public static RangeComparison FromSymbolToRangeComparison(string symbol)
        {
            if (!string.IsNullOrWhiteSpace(symbol))
            {
                switch (symbol)
                {
                    case ">":
                        return GreaterThan;
                    case "<":
                        return LessThan;
                    case ">=":
                        return GreaterThanOrEqual;
                    case "<=":
                        return LessThanOrEqual;
                }
                throw new System.Exception(nameof(symbol) + " not found" );
            }
            throw new ArgumentNullException( nameof(symbol) + " can not be null");
        }
    }
}
