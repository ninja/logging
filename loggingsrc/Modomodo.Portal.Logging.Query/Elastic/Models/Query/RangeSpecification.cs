﻿using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Query
{
    public class RangeSpecification
    {
        private readonly ElasticObject _value;
        private readonly RangeComparison _comp;

        public RangeSpecification(RangeComparison rangeComparison, ElasticObject value)
        {
            _comp = rangeComparison;
            _value = value;
        }

        public ElasticObject Value => _value;

        public RangeComparison RangeComparison => _comp;
    }
}
