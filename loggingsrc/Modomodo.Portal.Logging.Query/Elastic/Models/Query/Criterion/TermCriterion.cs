﻿using Modomodo.Portal.Logging.Query.Elastic.Models.Search;
using System.Text;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion
{
    public class TermCriterion : AbstractTermCriterion
    {
        public TermCriterion( ElasticProperty field, ElasticObject value): base( field, value)
        {
        }

        public string Name => "term";

        public override string ToQuery()
        {
            #region elastic query
            /*
                "term" : {
                    "field": "value"
                }
            */
            #endregion

            var query = new StringBuilder();
            query.AppendFormat("\"{0}\":{{", Name);
            query.AppendFormat("\"{0}\":\"{1}\"", Field.FieldName, Value.TrasformationValue(Field.Analyzer));            
            query.Append("}");            

            return query.ToString();
        }
    }
}
