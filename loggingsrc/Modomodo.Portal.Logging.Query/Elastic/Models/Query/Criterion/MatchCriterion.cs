﻿using Modomodo.Portal.Logging.Query.Elastic.Models.Search;
using System.Text;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion
{
    public class MatchCriterion : AbstractTermCriterion
    {
        public MatchCriterion (ElasticProperty field, ElasticObject value) : base(field, value)
        {
        }

        public string Name => "match";

        public override string ToQuery()
        {
            #region elastic query
            /***
                "query": {
                    "match": {
                        "field": value
                    }
                }
            ***/
            #endregion

            var query = new StringBuilder();
            query.Append("\"query\":{");
            query.AppendFormat("\"{0}\":{{", Name);
            query.AppendFormat("\"{0}\":\"{1}\"", Field.FieldName, Value.TrasformationValue(ElasticTypeAnalyzer.None));
            query.Append("}}");

            return query.ToString();
        }
    }
}
