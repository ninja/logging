﻿namespace Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion
{
    public class AndCriterion : CombineCriterion
    {
        public AndCriterion(AbstractCriterion leftCriterion, AbstractCriterion rightCriterion): base( leftCriterion, rightCriterion)
        {
        }

        public override string Name => "must";
    }
}
