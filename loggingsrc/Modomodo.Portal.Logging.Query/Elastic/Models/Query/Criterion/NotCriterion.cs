﻿using System.Text;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion
{
    public class NotCriterion : AbstractCriterion
    {
        private readonly AbstractCriterion _cr;
        public NotCriterion(AbstractCriterion criterion)
        {
            _cr = criterion;
        }

        public string Name => "must_not";

        public override string ToQuery()
        {
            #region elastic query
            /***
                "bool": {
                    "must_not" : [
                        { "filter": { "field": value}}
                    ]
                }
            ***/
            #endregion

            var query = new StringBuilder();
            query.Append("\"bool\":{");
            query.AppendFormat("\"{0}\":[{{", Name);
            query.Append(_cr.ToQuery());
            query.Append("}]}");
            return query.ToString();
        }
    }
}
