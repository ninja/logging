﻿namespace Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion
{
    public class OrCriterion : CombineCriterion
    {
        public OrCriterion(AbstractCriterion leftCriterion, AbstractCriterion rightCriterion) : base(leftCriterion, rightCriterion)
        {
        }
        public override string Name => "should";
    }
}
