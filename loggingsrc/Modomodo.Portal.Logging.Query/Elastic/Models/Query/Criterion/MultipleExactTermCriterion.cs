﻿using Modomodo.Portal.Logging.Query.Elastic.Models.Search;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion
{
    public class MultipleExactTermCriterion : AbstractCriterion
    {
        private readonly IList<ElasticObject> _values;
        private readonly ElasticProperty _field;

        public MultipleExactTermCriterion( ElasticProperty field, IList<ElasticObject> values)
        {
            _field = field;
            _values = values;
        }

        public ElasticProperty Field => _field;

        public ReadOnlyCollection<ElasticObject> Values => new ReadOnlyCollection<ElasticObject>(_values);

        public string Name => "terms";

        public override string ToQuery()
        {
            #region elastic query
            /***
            "terms":{
                "field": [value1, value2]
            }
            ***/
            #endregion

            var query = new StringBuilder();
            query.AppendFormat("\"{0}\":{{", Name);
            query.AppendFormat("\"{0}\":[", Field.FieldName);

            foreach (var v in _values.Take( _values.Count - 1).Select(value => value.TrasformationValue(Field.Analyzer)))
                query.AppendFormat("\"{0}\",", v);
            var lastValue = _values.Last();
            query.AppendFormat("\"{0}\"", lastValue.TrasformationValue(Field.Analyzer));
            
            query.Append("]}");
            return query.ToString();
        }
    }
}
