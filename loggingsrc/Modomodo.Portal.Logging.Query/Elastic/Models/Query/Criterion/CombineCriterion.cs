﻿using System.Text;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion
{
    public abstract class CombineCriterion : AbstractCriterion
    {
        private readonly AbstractCriterion _leftsc;
        private readonly AbstractCriterion _rightsc;
        protected CombineCriterion(AbstractCriterion leftCriterion, AbstractCriterion rightCriterion)
        {
            _leftsc = leftCriterion;
            _rightsc = rightCriterion;
        }

        public AbstractCriterion LeftCriterion => _leftsc;

        public AbstractCriterion RightCriterion => _rightsc;

        public override string ToQuery()
        {
            var query = new StringBuilder();
            query.Append("\"bool\":{");
            query.AppendFormat("\"{0}\":[{{", Name);
            query.Append(LeftCriterion.ToQuery());
            query.Append("},{");
            query.Append(RightCriterion.ToQuery());
            query.Append("}]}");            
            return query.ToString();
        }

        public abstract string Name { get; }
    }
}
