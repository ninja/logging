﻿using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Query;
using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Visitor;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion
{
    public abstract class AbstractCriterion : ISearchCriterion
    {
        public void Accept(IVisitor visitor)
        {
            visitor.Build(this);
        }
        
        public abstract string ToQuery();
    }
}
