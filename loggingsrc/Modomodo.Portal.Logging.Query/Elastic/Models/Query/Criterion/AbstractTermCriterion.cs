﻿using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Query;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion
{
    public abstract class AbstractTermCriterion : AbstractCriterion, ITermCriterion
    {
        private readonly ElasticProperty _field;
        private readonly ElasticObject _value;
        protected AbstractTermCriterion(ElasticProperty field, ElasticObject value)
        {
            _field = field;
            _value = value;
        }

        public ElasticProperty Field => _field;

        public ElasticObject Value => _value;
    }
}