﻿using System.Text;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion
{
    public class RangeCriterion : AbstractCriterion
    {
        private readonly ElasticProperty _field;
        private readonly RangeSpecification _firstSpec;
        private readonly RangeSpecification _secondSpec;

        public RangeCriterion(ElasticProperty field, RangeSpecification specification)
        {
            _field = field;
            _firstSpec = specification;
        }

        public RangeCriterion(ElasticProperty field, RangeSpecification firstSpec, RangeSpecification secondSpec)
        {
            _field = field;
            _firstSpec = firstSpec;
            _secondSpec = secondSpec;
        }

        public string Name => "range";

        public ElasticProperty Field => _field;

        public override string ToQuery()
        {
            #region elastic query
            /*"range" : {
                    "field" : {
                        "rangecomparison1" : value1,
                        "rangecomparison2" : value2
                    }
            }*/
            #endregion

            var query = new StringBuilder();
            query.AppendFormat("\"{0}\":{{", Name);
            query.AppendFormat("\"{0}\":{{", Field.FieldName);

            if (_secondSpec != null)
            {
                query.AppendFormat("\"{0}\":\"{1}\",", _firstSpec.RangeComparison.Value, _firstSpec.Value.TrasformationValue(Field.Analyzer));
                query.AppendFormat("\"{0}\":\"{1}\"", _secondSpec.RangeComparison.Value, _secondSpec.Value.TrasformationValue(Field.Analyzer));
            }
            else
                query.AppendFormat("\"{0}\":\"{1}\"", _firstSpec.RangeComparison.Value, _firstSpec.Value.TrasformationValue(Field.Analyzer));

            query.Append("}}");
            return query.ToString();
        }
    }
}
