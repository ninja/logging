﻿using Modomodo.Portal.Logging.Query.Elastic.Models.Search;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Configuration
{
    public class ConfigurationMappingWebMessageTemplate : AbstractConfigurationElasticMapping<WebMessageTemplate>
    {
        public ConfigurationMappingWebMessageTemplate()
        {
            SetMapping(x => x.level, ElasticTypeAnalyzer.None)
                .SetMapping(x => x.fields.messagetemplate.HttpRequest, ElasticTypeAnalyzer.Standard)
                .SetMapping(x => x.fields.messagetemplate.Keywords, ElasticTypeAnalyzer.Standard)
                .SetMapping(x => x.fields.messagetemplate.MachineName, ElasticTypeAnalyzer.None)
                .SetMapping(x => x.fields.messagetemplate.CodeResult, ElasticTypeAnalyzer.None)
                .SetMapping(x => x.fields.messagetemplate.UserAgent, ElasticTypeAnalyzer.Standard)
                .SetMapping(x => x.fields.messagetemplate.HttpRequestUrl, ElasticTypeAnalyzer.Simple)
                .SetMapping(x => x.fields.messagetemplate.HttpMethod, ElasticTypeAnalyzer.None)
                .SetMapping(x => x.fields.messagetemplate.Message, ElasticTypeAnalyzer.English)
                .SetMapping(x => x.fields.messagetemplate.ClientHostIp, ElasticTypeAnalyzer.None)
                .SetMapping(x => x.fields.messagetemplate.UserCorrelationId, ElasticTypeAnalyzer.None)
                .SetMapping(x => x.fields.messagetemplate.Exception.Message, ElasticTypeAnalyzer.Standard)
                .SetMapping(x => x.fields.messagetemplate.Exception._typeTag, ElasticTypeAnalyzer.Standard)
                .SetMapping(x => x.fields.messagetemplate.Exception.StackTrace, ElasticTypeAnalyzer.Simple)
                .SetMapping(x => x.fields.messagetemplate.Exception.TargetSite, ElasticTypeAnalyzer.Simple)
                .SetMapping(x => x.fields.messagetemplate.Exception.Source, ElasticTypeAnalyzer.Simple)
                .SetMapping(x => x.fields.messagetemplate.Exception.Data, ElasticTypeAnalyzer.Standard)
                .SetMapping(x => x.fields.messagetemplate.Exception.HelpLink, ElasticTypeAnalyzer.Standard)
                .SetMapping(x => x.fields.messagetemplate.Exception.InnerException, ElasticTypeAnalyzer.Standard);
        }
    }
}