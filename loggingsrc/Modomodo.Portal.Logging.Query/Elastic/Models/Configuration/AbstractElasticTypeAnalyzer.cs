﻿using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Configuration;
using Modomodo.Portal.Logging.Query.Elastic.Models.Search;
using System.Collections.Generic;
using System.Linq;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Configuration
{
    public abstract class AbstractElasticTypeAnalyzer
    {
        internal IList<ElasticMappingResult> Mapping = new List<ElasticMappingResult>();
        public IBaseElasticTypeAnalyzer GetTypeAnalyzerByField(string fieldName)
        {
            var mappingByProperty = Mapping.FirstOrDefault(x => x.FieldName == fieldName.ToLower());
            return mappingByProperty != null ? mappingByProperty.TypeAnalyzer : ElasticTypeAnalyzer.None;
        }
    }
}
