﻿using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Configuration;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Configuration
{
    internal class ElasticMappingResult
    {
        private readonly string _fieldName;
        private readonly IBaseElasticTypeAnalyzer _typeAnalyzer;

        public ElasticMappingResult(string fieldName, IBaseElasticTypeAnalyzer typeAnalyzer)
        {
            _fieldName = fieldName;
            _typeAnalyzer = typeAnalyzer;
        }

        public string FieldName => _fieldName;

        public IBaseElasticTypeAnalyzer TypeAnalyzer => _typeAnalyzer;
    }
}
