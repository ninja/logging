﻿using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Configuration;
using System;
using System.Linq.Expressions;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Configuration
{
    public abstract class AbstractConfigurationElasticMapping<T> : AbstractElasticTypeAnalyzer, IElasticMapping<T>
    {
        public IElasticMapping<T> SetMapping<TProperty>(Expression<Func<T, TProperty>> action, IBaseElasticTypeAnalyzer typeAnalyzer)
        {
            var expression = action.ExtractExpression();
            Mapping.Add(new ElasticMappingResult(expression.ToLower(), typeAnalyzer));
            return this;
        }
    }
}
