﻿using System;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Configuration
{
    public class ConfigurationMappingResult
    {
        public ConfigurationMappingResult(Type mappingType)
        {
            MappingType = mappingType;
        }

        public Type MappingType { get; private set; }
    }
}
