﻿using Modomodo.Portal.Logging.Query.Elastic.Interfaces.Visitor;
using Modomodo.Portal.Logging.Query.Elastic.Models.Query.Criterion;
using System.Text;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Visitor
{
    public class ElasticSearchQueryVisitor : IVisitor
    {
        public string Filter
        {
            get
            {
                var containerQuery = new StringBuilder();
                containerQuery.Append("{");
                containerQuery.Append(_filter);
                containerQuery.Append("}");
                return containerQuery.ToString();
            }
        }

        private string _filter;

        public void Build(AbstractCriterion searchCriterion)
        {
            var partQuery = searchCriterion.ToQuery();
            _filter += $" {partQuery} ";
        }
    }
}
