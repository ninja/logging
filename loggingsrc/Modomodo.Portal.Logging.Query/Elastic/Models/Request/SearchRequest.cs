﻿using System;
using System.Collections.Generic;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Request
{
    public abstract class SearchRequest<T> : BaseRequest
    {
        private int _maxSizeDocuments = 1000;
        private readonly List<SortOption<T>> _sortOptions;
        private string _filterRaw;

        protected SearchRequest(string indexName, string documentTypeName) : base( indexName, documentTypeName)
        {
            _sortOptions = new List<SortOption<T>>();
        }

        public int MaxSizeReturnDocuments
        {
            get
            {
                return _maxSizeDocuments;
            }
            set
            {
                _maxSizeDocuments = value;
            }
        }

        public IList<SortOption<T>> SortOptions => _sortOptions;

        public string FilterRaw => _filterRaw;

        public SearchRequest<T> SetSortOptions(IList<SortOption<T>> sortOptions)
        {
            if( sortOptions == null)
                throw new ArgumentNullException(nameof(sortOptions));
            _sortOptions.AddRange(sortOptions);
            return this;
        }

        public SearchRequest<T> SetFilterRaw( string filterRaw)
        {
            if (string.IsNullOrWhiteSpace(filterRaw))
                throw new ArgumentNullException(filterRaw);
            _filterRaw = filterRaw;
            return this;
        }

        protected abstract IList<SortOption<T>> GetDefaultSortOptions();
    }
}
