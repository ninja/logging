﻿using System;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Request
{
    public abstract class BaseRequest
    {
        private readonly string _indexName;
        private readonly string _documentTypeName;

        protected BaseRequest( string indexName, string documentTypeName)
        {
            if (string.IsNullOrWhiteSpace(indexName))
                throw new ArgumentNullException(indexName);
            if( string.IsNullOrWhiteSpace(documentTypeName))
                throw new ArgumentNullException(documentTypeName);

            _indexName = indexName;
            _documentTypeName = documentTypeName;
        }

        public string DocumentTypeName => _documentTypeName;

        public string IndexName => _indexName;
    }
}
