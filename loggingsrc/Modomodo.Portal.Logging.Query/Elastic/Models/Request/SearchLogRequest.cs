﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Request
{
    public class SearchLogRequest : SearchRequest<WebMessageTemplate>
    {
        private SearchLogRequest(string indexName, string documentTypeName) : base(indexName, documentTypeName)
        {
        }

        public static SearchLogRequest NewSearch( string indexName, string documentTypeName)
        {
            return new SearchLogRequest( indexName, documentTypeName);
        }

        public SearchLogRequest AddSize( int size)
        {
            MaxSizeReturnDocuments = size;
            return this;
        }

        public SearchLogRequest SetDefaultSortOption()
        {
            SetSortOptions(GetDefaultSortOptions());
            return this;
        }

        public SearchLogRequest SetSortOption<TProperty>(Expression<Func<WebMessageTemplate, TProperty>> action, bool ascending)
        {
            var sortOptions = new List<SortOption<WebMessageTemplate>>();
            var sort = SortOption<WebMessageTemplate>.CreateSort(action, ascending);
            sortOptions.Add(sort);
            SetSortOptions(sortOptions);
            return this;
        }

        protected override IList<SortOption<WebMessageTemplate>> GetDefaultSortOptions()
        {
            var defaultSortOptions = new List<SortOption<WebMessageTemplate>>();
            var sort = SortOption<WebMessageTemplate>.CreateSort(x => x.fields.messagetemplate.Date, false);
            defaultSortOptions.Add(sort);
            return defaultSortOptions;
        }
    }
}