﻿using System;
using System.Linq.Expressions;

namespace Modomodo.Portal.Logging.Query.Elastic.Models.Request
{
    public class SortOption<T>
    {
        private readonly string _fieldName;
        private readonly bool _ascending;

        private SortOption(string fieldName, bool ascending)
        {
            if (string.IsNullOrWhiteSpace(fieldName))
                throw new ArgumentNullException(nameof( fieldName));

            _fieldName = fieldName;
            _ascending = ascending;
        }

        public string FieldName => _fieldName;

        public bool Ascending => _ascending;

        public static SortOption<T> CreateSort<TProperty>(Expression<Func<T, TProperty>> action, bool ascending)
        {
            var expression = action.ExtractExpression();
            return new SortOption<T>(expression, ascending);
        }
    }
}