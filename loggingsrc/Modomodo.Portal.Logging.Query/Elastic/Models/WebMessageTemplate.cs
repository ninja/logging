﻿using System;
using System.Collections.Generic;

namespace Modomodo.Portal.Logging.Query.Elastic.Models
{
    public class fields
    {
        public fields()
        {
            messagetemplate = new messagetemplate();
        }

        public messagetemplate messagetemplate { get; set; }
    }

    public class messagetemplate
    {
        public messagetemplate()
        {
            Exception = new Exception();
        }

        public string HttpRequestUrl { get; set; }
        public IList<string> Keywords { get; set; }
        public string MachineName { get; set; }
        public string CodeResult { get; set; }
        public string UserAgent { get; set; }
        public string HttpRequest { get; set; }
        public string HttpMethod { get; set; }
        public string ClientHostIp { get; set; }
        public string UserCorrelationId { get; set; }
        public DateTimeOffset Date { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
    }

    public class Exception
    {
        public string Message { get; set; }
        public string _typeTag { get; set; }
        public string StackTrace { get; set; }
        public string Hresult { get; set; }
        public string TargetSite { get; set; }
        public string Source { get; set; }
        //TODO: da testare la serializzazione
        public object Data { get; set; }
        public string HelpLink { get; set; }
        public string InnerException { get; set; }
    }

    //TODO: Wrapper class WebMessageTemplate (Logging project)
    public class WebMessageTemplate
    {
        public WebMessageTemplate()
        {
            fields = new fields();
        }

        public fields fields { get; set; }

        public string level { get; set; }
    }
}
